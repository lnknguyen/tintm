//
//  TintmTests.swift
//  TintmTests
//
//  Created by Nguyen Luong on 6/17/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import XCTest
import Quick

@testable import Tintm

class TintmTests: XCTestCase {
    var vm : HomeViewModel?
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        vm = HomeViewModel(id: 12)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
            WebService.sharedInsance.queryForFeedsFromCategory([ 885000,884747,884534,884162,884705], offset: 0)
        }
    }
    
    func testViewModelValue() {
        let vc  = HomeViewController()
        XCTAssertNotNil(vc.view,"View did load did not load")
    }
    
}

class SubTests: QuickSpec{
    override func spec(){
    }
}
