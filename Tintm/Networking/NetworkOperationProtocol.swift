//
//  NetworkOperationProtocol.swift
//  Tintm
//
//  Created by Nguyen Luong on 8/1/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import PromiseKit
import Bond


protocol NetworkOperationProtocol {
    func getCategories(completionHandler:(ResultEntity<[CategoryEntity]>) -> Void)
    func getFeedsId(id: Int, completionHandler:(ResultEntity<[Int]>) -> Void)
    func getFeeds(id: Int, completionHandler: (ResultEntity<HomeNewsEntity>) -> Void)
    func getFeedDetail(id: Int, completionHandler: (ResultEntity<NewsEntity>) -> Void)
}

