//
//  NetworkManager.swift
//  Tintm
//
//  Created by Nguyen Luong on 8/1/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//
import PromiseKit
import Bond
struct NetworkManager : NetworkOperationProtocol {
    static let sharedInstance = NetworkManager()
    
    
    private init() {}
    /**
     Query categories
     
     - parameter completionHandler: handler
     */
    func getCategories(completionHandler:(ResultEntity<[CategoryEntity]>)->Void){
        WebService.sharedInsance.queryAllCategories()
            .then { (res) -> Void in
                let en = ResultEntity<[CategoryEntity]>(res: res)
                completionHandler(en)
            }.error { (err) in
                let en = ResultEntity<[CategoryEntity]>(error: err)
                completionHandler(en)
        }
    }
    
    /**
     Query feeds id by their category id
     
     - parameter id:                category id
     - parameter completionHandler: handler
     */
    
    func getFeedsId(id: Int, completionHandler: (ResultEntity<[Int]>) -> Void) {
        WebService.sharedInsance.queryHighlightNewsIdByCategoryId(id)
            .then { (res) -> Void in
                let en = ResultEntity<[Int]>(res:res)
                completionHandler(en)
            }.error { (err) in
                let en = ResultEntity<[Int]>(error: err)
                completionHandler(en)
        }
    }
    
    /**
     Query feed with detail omitted, pass it to a handler
     
     - parameter id:                feed's id
     - parameter completionHandler: handler
     */
    func getFeeds(id: Int, completionHandler: (ResultEntity<HomeNewsEntity>) -> Void) {
        WebService.sharedInsance.queryForSingleFeed(id)
            .then { (res) -> Void in
                let en = ResultEntity<HomeNewsEntity>(res: res)
                completionHandler(en)
            }.error { (err) in
                let en = ResultEntity<HomeNewsEntity>(error: err)
                completionHandler(en)
        }
    }
    
    /**
     Query feed detail
     
     - parameter id:                feed's id
     - parameter completionHandler: handle feed detail
     */
    func getFeedDetail(id: Int, completionHandler: (ResultEntity<NewsEntity>) -> Void) {
        WebService.sharedInsance.queryNewsDetailById(id)
            .then { (res) -> Void in
                let en = ResultEntity<NewsEntity>(res: res)
                completionHandler(en)
            }.error { (err) in
                let en = ResultEntity<NewsEntity>(error: err)
                completionHandler(en)
        }
    }
    
    /**
     Query for more feeds, maximum is 5
     Store feed in db, executed in background thread
     
     - parameter ids:               array of ids
     - parameter offset:            index to begin
     - parameter completionHandler: handle returned array
     */
 
    func getMoreFeeds(ids: [Int], offset: Int, completionHandler: (ResultEntity<[HomeNewsEntity]>) -> Void){
        WebService.sharedInsance.queryForMoreFeedsFromCategory(ids, offset: offset)
            .then { (res) -> [HomeNewsEntity] in
                let en = ResultEntity<[HomeNewsEntity]>(res: res)
                completionHandler(en)
                if let result =  en.res {
                    return result
                }
                return [HomeNewsEntity]()
            }.thenInBackground({ (entities) -> Void in
                let realmManager = RealmCommandManager()
                for entity in entities {
                    let realmHomeFeed = realmManager.create(entity)
                    realmManager.write(realmHomeFeed)
                }
            }).error { (err) in
                let en = ResultEntity<[HomeNewsEntity]>(error: err)
                completionHandler(en)
        }
    }
    
    /**
     Download and store news content in realm db, executed in background thread
     
     - parameter id: news id
     */
    func storeFeedInBackground(id: Int) {
        WebService.sharedInsance.queryNewsDetailById(id)
            .thenInBackground { (res) -> Void in
                let realmManager = RealmCommandManager()
                let realmNewsObj = realmManager.create(res)
                realmManager.write(realmNewsObj)
            }.error { (err) in
                print("Error storing feed in background")
        }
    }
    
    /**
     Query for related feeds of a feed
     
     - parameter id:                feed's id
     - parameter limit:             number of returned related feeds
     - parameter lastId:            use for paging, optional
     - parameter completionHandler: handlers
     */
    func getRelatedFeeds(id: Int, limit: Int = 3, lastId: Int = 0, completionHandler:(ResultEntity<[HomeNewsEntity]>) -> Void){
        WebService.sharedInsance.queryForRelatedFeeds(id,limit: limit, lastId: lastId)
        .then { (entities) -> Void in
            let en = ResultEntity<[HomeNewsEntity]>(res: entities)
            completionHandler(en)
        }.error { (err) in
            print(err)
            let en = ResultEntity<[HomeNewsEntity]>(error: err)
            completionHandler(en)
        }
    }
    
}
