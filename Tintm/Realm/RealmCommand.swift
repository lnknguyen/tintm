//
//  RealmCommand.swift
//  Tintm
//
//  Created by Nguyen Luong on 7/24/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import Foundation
import RealmSwift

enum RealmObjectType {
    case Home
    case News
    case Category
}

protocol Command {
    associatedtype Target
}

/**
 * Command to create new realm instances
 */
struct CreateCommand : Command {
    let realm = try! Realm()
    typealias Target = Entity
    
    func execute(entity: [Target]) -> [Object] {
        
        if let res = entity as? [HomeNewsEntity]{
            return res.map({ (e) -> RealmHomeObject in
                return RealmHomeObject(id: e.id, categoryId: e.categoryId, sourceName: e.sourceName, summary: e.summary, timestamp: e.timestamp, title: e.title, thumbnail: e.thumbnail, image: e.image, related: e.related, tintmUrl: e.tintmUrl)
            })
        } else if let res = entity as? [NewsEntity]{
            return res.map({ (e) -> RealmNewsObject in
                let homeObject = RealmHomeObject(id: e.id, categoryId: e.categoryId, sourceName: e.sourceName, summary: e.summary, timestamp: e.timestamp, title: e.title, thumbnail: "", image: "", related: 0, tintmUrl: e.tintmUrl)
                //Convert to html string before passing to realm object
                let htmlString = String.toHtml(e.title, sourceName: e.sourceName, timeStamp: e.timestamp, content: e.content)
                return RealmNewsObject(homeObject: homeObject, content: htmlString)
            })
        } else if let res = entity as? [CategoryEntity]{
            return res.map({ (e) -> RealmCategoryObject in
                return RealmCategoryObject(id: e.id, interest: e.interest, name: e.name, slug: e.slug)
            })
        }
        
        
        return [Object]()
    }
    
    func execute(entity: Target) -> Object {
        
        if let e = entity as? HomeNewsEntity{
            return RealmHomeObject(id: e.id, categoryId: e.categoryId, sourceName: e.sourceName, summary: e.summary, timestamp: e.timestamp, title: e.title, thumbnail: e.thumbnail, image: e.image, related: e.related, tintmUrl: e.tintmUrl)
            
        } else if let e = entity as? NewsEntity{
            let homeObject = RealmHomeObject(id: e.id, categoryId: e.categoryId, sourceName: e.sourceName, summary: e.summary, timestamp: e.timestamp, title: e.title, thumbnail: "", image: "", related: 0, tintmUrl: e.tintmUrl)
            //Convert to html string before passing to realm object
            let htmlString = String.toHtml(e.title, sourceName: e.sourceName, timeStamp: e.timestamp, content: e.content)
            return RealmNewsObject(homeObject: homeObject, content: htmlString)
        }else if let e = entity as? CategoryEntity{
            return RealmCategoryObject(id: e.id, interest: e.interest, name: e.name, slug: e.slug)
        }
        
        return Object()
    }
    
}

/**
 *  Command to read from realm database
 */
struct ReadCommand : Command {
    let realm = try! Realm()
    typealias Target = RealmObjectType
    
    
    func execute(type: Target,dictionary: [String:AnyObject]) -> [Object] {
        //assert(dictionary.count == 0,"Dictionary cannot be empty")
        if dictionary.count > 0 {
            let key = dictionary.keys.first!
            let value = dictionary[key]!
            
            switch type{
            case .Home:
                let predicate = NSPredicate(format: "%K == %@",argumentArray: [key,value])
                return Array(realm.objects(RealmHomeObject.self).filter(predicate))
            case .News:
                let predicate = NSPredicate(format: "%K == %@",argumentArray: [key,value])
                return Array(realm.objects(RealmNewsObject.self).filter(predicate))
            case .Category:
                let predicate = NSPredicate(format: "%K == %@",argumentArray: [key,value])
                return Array(realm.objects(RealmCategoryObject.self).filter(predicate))
            }
            
        }else{
            
            switch type{
            case .Home:
                return Array(realm.objects(RealmHomeObject.self))
            case .News:
                return Array(realm.objects(RealmNewsObject.self))
            case .Category:
                return Array(realm.objects(RealmCategoryObject.self))
            }
        }
    }
    
    func execute(type: Target,dictionary: [String:AnyObject]) -> Bool{
        let key = dictionary.keys.first!
        let value = dictionary[key]!
        
        
        switch type{
        case .Home:
            let predicate = NSPredicate(format: "%K == %@",argumentArray: [key,value])
            return Array(realm.objects(RealmHomeObject.self).filter(predicate)).count > 0
        case .News:
            let predicate = NSPredicate(format: "%K == %@",argumentArray: [key,value])
            return Array(realm.objects(RealmNewsObject.self).filter(predicate)).count > 0
        case .Category:
            let predicate = NSPredicate(format: "%K == %@",argumentArray: [key,value])
            return Array(realm.objects(RealmCategoryObject.self).filter(predicate)).count > 0
        }
    }
    
    func executeSearch(type: Target, dictionary: [String:AnyObject])->[Object]{
        let key = dictionary.keys.first!
        let value = NSMutableString(string: dictionary[key] as! String)
        switch type{
        case .Home:
            let predicate = NSPredicate(format: "%K CONTAINS[c] %@",argumentArray: [key,value])
            return Array(realm.objects(RealmHomeObject.self).filter(predicate))
        case .News:
            let predicate = NSPredicate(format: "%K == %@",argumentArray: [key,value])
            return Array(realm.objects(RealmNewsObject.self).filter(predicate))
        case .Category:
            let predicate = NSPredicate(format: "%K == %@",argumentArray: [key,value])
            return Array(realm.objects(RealmCategoryObject.self).filter(predicate))
        }
    }
    
}

/**
 *  Command to write to realm database
 */
struct WriteCommand : Command {
    let realm = try! Realm()
    typealias Target = Object
    
    func execute(object: Target) {
        try! realm.write({
            realm.add(object, update: true)
        })
    }
    
    func execute(objects: [Target]) {
        for e in objects{
            try! realm.write({
                realm.add(e, update: true)
            })
        }
    }
}

/**
 *  Command to delete objects from realm database
 */
struct DeleteCommand : Command {
    let realm = try! Realm()
    typealias Target = RealmObjectType
    
    func execute(type: Target){
        try! realm.write({
            switch type{
            case .Home:
                let objects = realm.objects(RealmHomeObject.self)
                realm.delete(objects)
            case .News:
                let objects = realm.objects(RealmNewsObject.self)
                realm.delete(objects)
            case .Category:
                let objects = realm.objects(RealmCategoryObject.self)
                realm.delete(objects)
            }
        })
    }
    
    func execute(type: Target, dictionary: [String:AnyObject]){
        try! realm.write({
            let key = dictionary.keys.first!
            let value = dictionary[key]
            let predicate = NSPredicate(format: "%@ = %@",key,value as! String)
            switch type{
            case .Home:
                let objects = Array(realm.objects(RealmHomeObject.self).filter(predicate))
                realm.delete(objects)
            case .News:
                let objects = Array(realm.objects(RealmNewsObject.self).filter(predicate))
                realm.delete(objects)
            case .Category:
                let objects = Array(realm.objects(RealmCategoryObject.self).filter(predicate))
                realm.delete(objects)
            }
            
        })
    }
    
    func executeClearingByDate(type: Target, value: Int){
        try! realm.write({
            switch type {
            case .Home:
                let objects = Array(realm.objects(RealmHomeObject.self))
                for obj in objects {
                    if (obj.timestamp.toDate().isGreaterThan(NSDate(), unit: NSCalendarUnit.Day, amount: value)
                        && !obj.isBookmark){
                        realm.delete(obj)
                    }
                }
            case .News:
                let objects = Array(realm.objects(RealmNewsObject.self))
                
                for obj in objects {
                    
                    if let homeObject = obj.homeObject {
                        if (homeObject.timestamp.toDate().isGreaterThan(NSDate(), unit: NSCalendarUnit.Day, amount: value)
                            && !homeObject.isBookmark){
                            realm.delete(obj)
                        }
                    }
                }
            case .Category:
                fatalError("Cant  clearing by date")
            }
        })
    }
}

/**
 *  Command to update an element
 */
struct UpdateCommand : Command {
    let realm = try! Realm()
    typealias Target = Void
    
    func execute(closure: Target -> Void){
        realm.beginWrite()
        closure()
        try! realm.commitWrite()
    }
    
    
}
