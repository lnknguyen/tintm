//
//  RealmOperationManager.swift
//  Tintm
//
//  Created by Nguyen Luong on 7/24/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import Foundation
import RealmSwift

//Using command pattern 
//Visit https://github.com/ochococo/Design-Patterns-In-Swift#behavioral for more info

protocol RealmCommandProtocol{
    func write(object: Object)
    func read(type: RealmObjectType, dictionary: [String:AnyObject]) -> [Object]
    func create(entity: [Entity]) -> [Object]
}

struct RealmCommandManager: RealmCommandProtocol {
    private let writeCommand = WriteCommand()
    private let readCommand = ReadCommand()
    private let createCommand = CreateCommand()
    private let deleteCommand = DeleteCommand()
    private let updateCommand = UpdateCommand()
    /**
     Create an array of realm objects from an array of entity objects
     
     - parameter entity: array of entity
     
     - returns: array of realm object
     */
    
    func create(entities: [Entity]) -> [Object]{
        return createCommand.execute(entities)
    }
    
    /**
     Create a single realm object from an entity
     
     - parameter entity: entity object
     
     - returns: realm object
     */
    func create(entity: Entity) -> Object{
        return createCommand.execute(entity)
    }
 
    /**
     Write a realm object into database
     
     - parameter object: any realm object
     */
    func write(object: Object){
        writeCommand.execute(object)
    }
    
    /**
     Write an array of realm objects into database
     
     - parameter object: array of realm objects
     */
    func write(objects: [Object]){
        writeCommand.execute(objects)
    }
    
    /**
     Query a list of realm objects from database
     
     - parameter type: Type of object, defined by RealmObjectType enum
     - parameter key:  key to query, should be id or categoryId
     
     - returns: a list of realm objects
     */
    func read(type: RealmObjectType, dictionary: [String:AnyObject]) -> [Object]{
        return readCommand.execute(type,dictionary: dictionary)
    }
    
    
    func search(type: RealmObjectType, dictionary: [String:AnyObject]) -> [Object]{
        return readCommand.executeSearch(type, dictionary: dictionary)
    }
    /**
     Check if an object is in realm database
     
     - parameter type:       Type of object, defined by RealmObjectType enum
     - parameter dictionary: key to query, should be id or categoryId
     
     - returns: true if object exists, false otherwise
     */
    func contains(type: RealmObjectType, dictionary: [String:AnyObject]) -> Bool{
        return readCommand.execute(type, dictionary: dictionary)
    }
    
    /**
     Delete all objects from a given type
     - parameter type: type of realm objects, given by realmobjecttype enum
     */
    func clear(type: RealmObjectType){
        deleteCommand.execute(type)
    }
    
    /**
     Delete all objects satisfied equal predication from a give type
     - parameter type: type of realm objects, given by realmobjecttype enum
     */
    func delete(type: RealmObjectType, dictionary: [String:AnyObject]){
        deleteCommand.execute(type,dictionary: dictionary)
    }
    
    /**
     Delete all objects that are created before date
     
     - parameter type:  realm object type
     - parameter value: amount of days
     
     E.g: value = 3 -> all feeds that are created before 3 days ago
                        will be deleted
     */
    func clearAllFeedsBeforeDate(type: RealmObjectType, value: Int){
        deleteCommand.executeClearingByDate(type, value: value)
    }
    /**
     Perform update on a list of objects, specified inside a closure
     
     - parameter closure: operation to be performed during an update
     */
    func update(closure: Void -> Void){
        updateCommand.execute(closure)
    }
    
    
}