//
//  HomeCellTypeTwo.swift
//  Tintm
//
//  Created by Nguyen Luong on 6/26/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import UIKit
import Cartography
import Bond



class BaseHomeCell: UITableViewCell{
    
    
    var delegate : ButtonDelegate?
    var viewModel = HomeViewModel()
    var state = CellState.Collapsed
    var imgView  = UIImageView()
    var titleView = UILabel()
    var summaryView = UILabel()
    var sourceView = UILabel()
    var timestampView = UILabel()
    var expandButton = UIButton()
    var group = ConstraintGroup()
    //var imageRequest : ImageRequest?
    
    typealias HomeCellPresentable = protocol<TextPresentable,ImagePresentable>
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: "")
        self.selectionStyle = .None
        
        self.addSubview(imgView)
        
        self.addSubview(titleView)
        self.addSubview(sourceView)
        self.addSubview(timestampView)
    
        configureSubview(viewModel)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse() 
        imgView.image = nil
        titleView.text = ""
        sourceView.text = ""
        timestampView.text = ""
        summaryView.attributedText = NSMutableAttributedString(string: "")
        
        delegate = nil
        if state == .Expanded {
            collapseView()
        }
    }
    
    func constrainSubview(ratio: CGFloat = 1.0){
        preconditionFailure("Subclass must implement this method")
    }
    
    func configureSubview(presenter: HomeCellPresentable){
        //Set subviews background color to white to avoid blending operation in cell
        //See more: https://medium.com/ios-os-x-development/perfect-smooth-scrolling-in-uitableviews-fd609d5275a5#.ymbe3iumc
        
        titleView.font = presenter.headingFont
        summaryView.font = presenter.summaryFont
        sourceView.font = presenter.sourceNameFont
        timestampView.font = presenter.timeStampFont
        
        sourceView.textColor = presenter.sourceNameColor
        timestampView.textColor = presenter.timeStampColor
        
        for subview in self.subviews{
            if subview is UILabel{
                if let subview = subview as? UILabel
                {
                    subview.numberOfLines = 0
                    subview.lineBreakMode = .ByTruncatingTail
                    subview.sizeToFit()
                    subview.backgroundColor = UIColor.whiteColor()
                }
            }
        }
        
        summaryView.numberOfLines = 0
        summaryView.lineBreakMode = .ByTruncatingTail
        summaryView.sizeToFit()
        summaryView.backgroundColor = UIColor.whiteColor()
        
        imgView.backgroundColor = UIColor.lightGrayColor()
        imgView.layer.borderWidth = 2.0
        imgView.layer.borderColor = UIColor.silverColor().CGColor
        
        expandButton.setImage(UIImage(named: ImageName.ExpandIcon.description), forState: .Normal)
        expandButton.imageView!.backgroundColor = UIColor.clearColor()
        expandButton.imageEdgeInsets = UIEdgeInsetsMake(20, APP_WIDTH-40, 5, 5)
        expandButton.bnd_tap.observe { [unowned self] in
            if let _delegate = self.delegate{
                _delegate.didTapButtonInsideCell(self)
            }
        }

        
    }
    
    func publishData(feed: HomeNewsEntity){
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 10
        let attributedString = NSMutableAttributedString(string: feed.summary)
        attributedString.addAttribute(NSParagraphStyleAttributeName, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        
        timestampView.text = feed.timestamp.timePassed
        titleView.text = feed.title
        summaryView.attributedText = attributedString
        sourceView.text = feed.sourceName
    }
    
    func publishData(feed: RealmHomeObject){
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 10
        let attributedString = NSMutableAttributedString(string: feed.summary)
        attributedString.addAttribute(NSParagraphStyleAttributeName, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        
        timestampView.text = feed.timestamp.timePassed
        titleView.text = feed.title
        summaryView.attributedText = attributedString
        sourceView.text = feed.sourceName
    }
    
    func expandView() {
        state.switchState()
        expandButton.setImage(UIImage(named: ImageName.CollapseIcon.description), forState: .Normal)
        expandButton.imageEdgeInsets = UIEdgeInsetsMake(20, APP_WIDTH-40, 10, 5)
    }
    
    
    func collapseView() {
        state.switchState()
        expandButton.setImage(UIImage(named: ImageName.ExpandIcon.description), forState: .Normal)
        expandButton.imageEdgeInsets = UIEdgeInsetsMake(25, APP_WIDTH-40, 5, 5)
    }
    
    
    func preheatImage(entity: HomeNewsEntity){
        
    }
    
   }
