//
//  HomeCellTypeTwo.swift
//  Tintm
//
//  Created by Nguyen Luong on 6/27/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import Foundation

import UIKit
import Cartography
import Kingfisher

@IBDesignable
class HomeCellTypeTwo: BaseHomeCell{
    var ratio : CGFloat = 0
    typealias HomeCellPresentable = protocol<TextPresentable,ImagePresentable>
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: HOME_CELL_TYPE_TWO_ID)
        //self.addSubview(summaryView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    //initial constraint
    override func constrainSubview(ratio: CGFloat){
        //capture constraint and store into group
        
        constrain(imgView, titleView, sourceView, timestampView, replace: group) { (view1, view2, view3, view4) in
            guard let superview = view1.superview else { return }
            view1.top == superview.top
            view1.left == superview.left
            view1.right == superview.right
            (view1.height <= view1.width * ratio) ~ 800
            
            view2.top == view1.bottom + 5
            view2.left == view1.left + 10
            view2.right == view1.right - 10
            
            view3.top == view2.bottom + 5
            view3.left == view2.left
            
            view4.top == view3.top
            view4.left == view3.right + 5
            
            view3.bottom <= superview.bottom - 5
            view4.bottom == view3.bottom
            
        }
        
    }
    
    override func publishData(feed: HomeNewsEntity) {
        super.publishData(feed)
        self.constrainSubview(1.0)
        if let url = NSURL(string: feed.image) {
            imgView.kf_setImageWithURL(url, placeholderImage: nil, optionsInfo: [.Transition(ImageTransition.Fade(1)),.DownloadPriority(1.0)], progressBlock: { (receivedSize, totalSize) in
                
                }, completionHandler: { (image, error, cacheType, imageURL) in
                    if let _image = image {
                        let ratio = _image.size.height / _image.size.width
                        self.constrainSubview(ratio)
                        self.imgView.image = image
                    }else {
                        self.constrainSubview(0.0)
                    }
                    
            })
        }
    }
    
    override func publishData(feed: RealmHomeObject) {
        super.publishData(feed)
    }
    
    
    override func configureSubview(presenter: HomeCellPresentable) {
        super.configureSubview(presenter)
        
    }
    
    
    
    
}