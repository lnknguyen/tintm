//
//  HomeCell.swift
//  Tintm
//
//  Created by Nguyen Luong on 6/17/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//


import UIKit
import Cartography
import Kingfisher

@IBDesignable
class HomeCellTypeOne: BaseHomeCell{
    var width : CGFloat = 0
    
    
    typealias HomeCellPresentable = protocol<TextPresentable,ImagePresentable>
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: HOME_CELL_TYPE_ONE_ID)
        self.addSubview(expandButton)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    override func constrainSubview(ratio: CGFloat = 1.0){
        constrain(imgView, titleView, sourceView, timestampView, expandButton, replace: group) { (view1, view2, view3, view4, view5) in
            guard let superview = view1.superview else { return }
            view1.top == superview.top + 10
            view1.left == superview.left + 10
            view1.width == APP_WIDTH * 0.35
            view1.height == APP_WIDTH * 0.25
            
            view2.top == view1.top
            view2.right == superview.right - 10
            view2.left == view1.right + 5
            
            view3.top == view2.bottom + 5
            view3.left == view2.left
            view3.height == 15
            
            view4.top == view3.top
            view4.left == view3.right + 5
            view4.bottom == view3.bottom
            
            
            view5.right == superview.right
            view5.left == superview.left
            view5.height == 40
            
            (view1.bottom <= superview.bottom - 5) ~ 100
            (view3.bottom <= superview.bottom - 5) ~ 100
            (view5.bottom <= superview.bottom - 5) ~ 100
        }
    }
    
    override func publishData(feed: HomeNewsEntity) {
        super.publishData(feed)
        self.constrainSubview()
        if let url = NSURL(string: feed.thumbnail) {
            imgView.kf_setImageWithURL(url, placeholderImage: nil, optionsInfo: [.Transition(ImageTransition.Fade(1)),.DownloadPriority(0.2)], progressBlock: { (receivedSize, totalSize) in

                }, completionHandler: { (image, error, cacheType, imageURL) in
                    self.imgView.image = image
                    self.constrainSubview()
            })
        }
    }
    
    override func publishData(feed: RealmHomeObject) {
        super.publishData(feed)
    }
    
    override func configureSubview(presenter: HomeCellPresentable) {
        super.configureSubview(presenter)
        imgView.contentMode = .ScaleToFill
    }
    
    //Expand and collapse cell
    override func expandView() {
        super.expandView()
        self.addSubview(summaryView)
        summaryView.alpha = 0
        constrain(imgView, summaryView,sourceView, expandButton) { (view1, view2, view3,view4) in
            guard let superview = view1.superview else { return }
            //view2.top == (view1.bottom + 10) ~ 400
            view2.top == (view1.bottom + 15) ~ 400
            view2.left == superview.left + 5
            view2.right == superview.right - 5
            view2.bottom <= (superview.bottom - 5) ~ 600
            
            view4.top == view2.bottom + 5
            view4.bottom <= (superview.bottom - 5) ~ 800
        }
        
        UIView.animateWithDuration(0.5) {
            self.summaryView.alpha = 1
        }
    }
    
    override func collapseView() {
        super.collapseView()
        summaryView.removeFromSuperview()
        constrain(imgView){view1 in
            view1.bottom <= (view1.superview?.bottom)! - 5
        }
    }
    
    
}

