//
//  HomeDataSourceAndDelegate.swift
//  Tintm
//
//  Created by Nguyen Luong on 7/8/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import UIKit


extension HomeViewController: UITableViewDataSource, ButtonDelegate{
    //MARK: - Table Data source
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (viewModel?.isOffline == true){
            return viewModel!.offlineDataSource.count
        }
        return viewModel!.observableFeeds.count
        //return viewModel.realmArray().count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let inpRow = indexPath.row
        if (viewModel?.isOffline == false){
            if let id = viewModel?.observableIds.value[inpRow]{
                NetworkManager.sharedInstance.storeFeedInBackground(id)
            }
            var returnCell = BaseHomeCell()
            if (inpRow % 7 == 0){
                let cell = (tableView.dequeueReusableCellWithIdentifier(HOME_CELL_TYPE_TWO_ID, forIndexPath: indexPath) as? HomeCellTypeTwo)!
                returnCell = cell
            }else{
                let cell = (tableView.dequeueReusableCellWithIdentifier(HOME_CELL_TYPE_ONE_ID, forIndexPath: indexPath) as? HomeCellTypeOne)!
                returnCell  = cell
            }
            let entity = viewModel?.observableFeeds[inpRow]
            returnCell.viewModel = viewModel!
            returnCell.delegate = self
            if let _entity = entity{
                returnCell.publishData(_entity)
                //returnCell.constrainSubview()
            }
            //viewModel?.getFeed(id!, cell: returnCell)
            return returnCell
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier(BOOKMARK_CELL_ID,forIndexPath: indexPath) as? BookmarkCell
            let entity = viewModel?.offlineDataSource[inpRow]
            cell?.viewModel = viewModel!
            cell?.publishData(entity!)
            cell?.constrainSubview()
            return cell!
        }
    }
    
    //Delegate method for button
    func didTapButtonInsideCell(sender: UITableViewCell) {
        let cell = sender as! BaseHomeCell
        tableView.beginUpdates()
        switch cell.state{
        case .Expanded:
            cell.collapseView()
        case .Collapsed:
            cell.expandView()
            
        }
        tableView.layoutIfNeeded()
        tableView.endUpdates()
    }
    
}

extension HomeViewController: UITableViewDelegate{
    //MARK: - Table Delegate
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if (indexPath.row  == (viewModel?.observableFeeds.count)!-7){
            viewModel?.loadMore(indexPath.row+7)
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //router?.pushFromHomeViewToArticle(indexPath.row)
        router?.pushFromHomeViewToPageView(indexPath.row)
    }
    
    //Scroll to top button pops up when user scroll pass 1.5 * screen's height
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let threshold = APP_HEIGHT*1.5
        if (scrollView.contentOffset.y > threshold){
            UIView.animateWithDuration(1, animations: {
                self.scrollToTopButton.center = self.onScreenCenter
            })
        }else {
            UIView.animateWithDuration(1, animations: {
                self.scrollToTopButton.center = self.offScreenCenter
            })
        }
        
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return footerRefresh
    }
    
}

extension HomeViewController: HomeAdapter{
    //Successfully referesh list of news id
    func successRefreshIds(res: [Int], first: Bool) {
        if first{
            viewModel?.loadMore(0)
        } else {
            tableView.reloadData()
            refreshControl.endRefreshing()
            AlertViewManager.sharedInstance.stopLoading()
        }
        
    }
    
    
    
    //Successfully load more feeds
    func successLoadMore(res: [HomeNewsEntity])
    {
        tableView.reloadData()
        refreshControl.endRefreshing()
        AlertViewManager.sharedInstance.stopLoading()
    }
    
    //Successfully load a single feed
    func successLoadFeed(res: HomeNewsEntity){
        let realmNews = viewModel?.realmManager.create(res)
        if let news = realmNews{
            viewModel?.realmManager.write(news)
            refreshControl.endRefreshing()
        }
        
    }
    
    //Successfully load entity into cell
    func successHandler(res: HomeNewsEntity, cell: BaseHomeCell){
        cell.preheatImage(res)
        refreshControl.endRefreshing()
        AlertViewManager.sharedInstance.stopLoading()
    }
    
    //Successfully load realm data into cell
    func successRefreshRealm(res: RealmHomeObject, cell: BaseHomeCell){
        cell.publishData(res)
        cell.constrainSubview()
        refreshControl.endRefreshing()
        AlertViewManager.sharedInstance.stopLoading()
    }
    
    //Failure with error
    func failureHandler(err: ErrorType) {
        print(err)
        refreshControl.endRefreshing()
        let emptyView = StateView(frame: CGRectMake(0, 0, APP_WIDTH, APP_HEIGHT), state: .Empty)
        tableView.backgroundView = emptyView
        tableView.separatorColor = UIColor.clearColor()
        AlertViewManager.sharedInstance.stopLoading()
    }
}




