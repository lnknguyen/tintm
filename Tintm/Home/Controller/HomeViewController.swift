//
//  HomeViewController.swift
//  Tintm
//
//  Created by Nguyen Luong on 6/17/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//


import UIKit


import Cartography



//TODO: Temporarily disable pull to refresh due to conflict in logic
class HomeViewController: UIViewController  {
    var viewModel : HomeViewModel?
    let tableView = UITableView()
    let scrollToTopButton = UIButton(frame: CGRectMake(0,0,30,30))
    let refreshControl = UIRefreshControl()
    let offScreenCenter = CGPointMake(APP_WIDTH*4/5, APP_HEIGHT+100)
    let onScreenCenter = CGPointMake(APP_WIDTH*4/5, APP_HEIGHT-100)
    let realmManager = RealmCommandManager()
    var router : HomeViewRouter?
    var footerRefresh = UIRefreshControl()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(tableView)
        tableView.addSubview(refreshControl)
        //tableView.addSubview(footerRefresh)
        self.view.addSubview(scrollToTopButton)
        
        
        constrain(tableView) { (tableView) in
            tableView.top == (tableView.superview?.top)! + 1
            tableView.left == (tableView.superview?.left)! + 1
            tableView.right == (tableView.superview?.right)! - 1
            tableView.bottom == (tableView.superview?.bottom)! - 20
        }
        
        tableView.registerClass(HomeCellTypeOne.self, forCellReuseIdentifier: HOME_CELL_TYPE_ONE_ID)
        tableView.registerClass(HomeCellTypeTwo.self, forCellReuseIdentifier: HOME_CELL_TYPE_TWO_ID)
        tableView.registerClass(BookmarkCell.self, forCellReuseIdentifier: BOOKMARK_CELL_ID)
        viewModel?.delegate = self
        configureSubviews()
        bindViewModel(true) //first time binding
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        //bindViewModel()
    }
    
    
    func refresh(){
        bindViewModel()
    }
    
    
    /**
     Binding view model to controller
     
     - parameter first: true if first time binding
     */
    private func bindViewModel(first: Bool = false){
        if (viewModel?.isOffline == false){
            viewModel?.refreshFeedsId(first)
        
        }else {
            viewModel?.offlineLoad()
            self.refreshControl.endRefreshing()
            AlertViewManager.sharedInstance.stopLoading()
        }
    }
    
    
    func configureSubviews(){
        
        tableView.delegate = self
        tableView.dataSource = self
    
        tableView.showsVerticalScrollIndicator = false
        tableView.alwaysBounceVertical = false
        
        tableView.estimatedRowHeight = APP_HEIGHT/5
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.setNeedsLayout()
        
        
        refreshControl.addTarget(self, action: #selector(refresh), forControlEvents: .ValueChanged)
        
        scrollToTopButton.setImage(UIImage(named:ImageName.ScrollToTopIcon.description), forState: .Normal)
        scrollToTopButton.center = offScreenCenter
        
        scrollToTopButton.bnd_tap.observe {
            self.tableView.scrollToRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0), atScrollPosition: .Top, animated: true)
        }
    }
    
    
}




