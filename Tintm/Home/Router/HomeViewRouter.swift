//
//  HomeViewRouter.swift
//  Tintm
//
//  Created by Nguyen Luong on 7/29/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import Foundation

protocol HomeViewRouterProtocol {
    func pushFromHomeViewToArticle(row: Int)
    func pushFromHomeViewToPageView(row: Int)
}

