//
//  HomeViewModel.swift
//  Tintm
//
//  Created by Nguyen Luong on 6/17/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import PromiseKit
import Bond
import RealmSwift

protocol HomeAdapter {
    func successRefreshIds(res: [Int], first: Bool )
    func successLoadMore(res: [HomeNewsEntity])
    func successLoadFeed(res: HomeNewsEntity)
    func successHandler(res: HomeNewsEntity, cell: BaseHomeCell)
    func successRefreshRealm(res: RealmHomeObject, cell: BaseHomeCell)
    func failureHandler(err: ErrorType)
}

struct HomeViewModel:TextPresentable, ImagePresentable{
    var id: Int //category's id
    var name = "" // category's name
    let realmManager = RealmCommandManager()
    var observableFeeds : ObservableArray<HomeNewsEntity>
    var observableIds : Observable<[Int]>
    var currIds : [Int]
    var delegate : HomeAdapter?
    var isOffline : Bool = false
    var offlineDataSource = [RealmHomeObject]()
    
    init(id: Int = 0){
        self.id = id
        observableFeeds = ObservableArray()
        observableIds = Observable([])
        currIds = []
        isOffline = false
    }
    
    init(id: Int, name: String){
        self.init(id: id)
        self.name = name
    }
    
    init(id: Int, name: String, isOffline: Bool){
        self.init(id: id,name: name)
        self.isOffline = isOffline
    }
    /**
     Refresh list of news id
     */
    func refreshFeedsId(first: Bool = false){
        NetworkManager.sharedInstance.getFeedsId(id) { (res) in
            switch res.status{
            case .Success:
                if let value = res.res{
                    self.observableIds.next(value)
                    self.observableIds.value.sortInPlace({
                        return $0 > $1
                    })
                    self.delegate?.successRefreshIds(value,first: first)
                }
                else {
                    self.delegate?.failureHandler(ErrorCode.UnknownError)
                }
            case .Failure:
                self.delegate?.failureHandler(res.error!)
            }
        }
    }
    
    /**
     Get a single feed
     */
    func getFeed(feedId: Int, cell: BaseHomeCell){
        NetworkManager.sharedInstance.getFeeds(feedId) { (res) in
            switch res.status{
            case .Success:
                if let value = res.res{
                    let realmNews = self.realmManager.create(value)
                    self.realmManager.write(realmNews)
                    self.delegate?.successRefreshRealm(realmNews as! RealmHomeObject,cell: cell)
                }
            case .Failure:
                if let err = res.error{
                    print(res.error)
                    self.delegate?.failureHandler(err)
                }
                
            }
        }
    }
    
    /**
     Load 5 more feeds
     
     - parameter offset: index to begin from
     */
    func loadMore(offset: Int){
        NetworkManager.sharedInstance.getMoreFeeds(observableIds.value, offset: offset) { (res) in
            switch res.status{
            case .Success:
                if let value = res.res{
                    self.observableFeeds.extend(value)
                    self.delegate?.successLoadMore(value)
                }
            case .Failure:
                if let err = res.error{
                  self.delegate?.failureHandler(err)
                }
            }
        }
    }
    /**
     Offline loading
     */
    mutating func offlineLoad(){
        offlineDataSource = realmManager.read(.Home, dictionary: ["categoryId":id]) as! [RealmHomeObject]
        offlineDataSource.sortInPlace({$0.0.timestamp.toDate().compare($0.1.timestamp.toDate()) == .OrderedDescending})
    }
    
    
}




