//
//  SettingViewController.swift
//  Tintm
//
//  Created by Nguyen Luong on 7/29/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import UIKit

class SettingViewController : UITableViewController {
    var viewModel : SettingViewModel?
    var router: SettingViewRouter?
    
    override init(style: UITableViewStyle) {
        super.init(style: style)
        tableView.registerClass(SettingCell.self, forCellReuseIdentifier: SETTING_CELL_ID)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    convenience init(){
        self.init(style: .Grouped)
    }
    
    override func viewWillAppear(animated: Bool) {
        navigationController?.navigationBarHidden = false
    }
    
    //MARK: Delegate and data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (viewModel?.menu[section].count)!
    }
    
   
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let section = indexPath.section
        let row = indexPath.row
        let cell = tableView.dequeueReusableCellWithIdentifier(SETTING_CELL_ID) as! SettingCell
        
        cell.textLabel?.text = viewModel?.menu[section][row]
        cell.configureWithPresenter(viewModel!)
        return cell
    }
    
    //Rework later : Add more clarification ( replace number with enum ? )
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch indexPath.section{
        case 0:
            switch indexPath.row{
            case 0:
                router!.pushToWebsiteView()
            case 1:
                router!.pushToLibraryView()
            case 2:
                break
            case 3:
                break
            case 4:
                router?.pushToMailView()
            default:
                return
            }
        default:
            return
        }
    }
    
    
}

