//
//  SettingRouter.swift
//  Tintm
//
//  Created by Nguyen Luong on 7/30/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import Foundation

protocol SettingRouterProtocol {
    func pushToLibraryView()
    func pushToWebsiteView()
    func pushToMailView()
}