//
//  MailViewController.swift
//  Tintm
//
//  Created by Nguyen Luong on 7/30/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import UIKit
import MessageUI

class MailViewController : MFMailComposeViewController, MFMailComposeViewControllerDelegate
{
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    convenience init(){
        self.init(nibName:nil,bundle: nil)
        self.mailComposeDelegate = self
        self.setToRecipients(["info@tintm.com"])
        self.setSubject("Phản hồi")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //Delegate
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        if (error != nil){
            print(error)
        }
        
        switch result {
        case MFMailComposeResultSent:
            AlertViewManager.sharedInstance.showSuccess(.MailSent)
        case MFMailComposeResultFailed:
            AlertViewManager.sharedInstance.showFailure(.MailFailed)
        default:
            break
            
        }
        
        dismissViewControllerAnimated(true, completion: nil)
    }
}