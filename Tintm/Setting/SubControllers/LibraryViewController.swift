//
//  LibraryViewController.swift
//  Tintm
//
//  Created by Nguyen Luong on 7/30/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import UIKit
import Cartography
import Bond
import WebKit
import RealmSwift

class LibraryViewController: UIViewController{
    
    var router : ArticleViewRouter?
    
    var webView = WKWebView()
    let backButton = UIButton(frame: CGRectMake(0,0,25,25))
    let bundle = NSBundle.mainBundle()
    var path = ""
    
    typealias ArticleViewPresenter = protocol<TextPresentable,ImagePresentable>
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.whiteColor()
        path =  bundle.pathForResource("default", ofType: "css")!
        configureSubviews()
        constrainSubviews()
        let url = NSBundle.mainBundle().URLForResource("library", withExtension:"html")
        let request = NSURLRequest(URL: url!)
        webView.loadRequest(request)
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBarHidden = false
    }
    
    
    
    
    func constrainSubviews(){
        view.addSubview(webView)
        constrain(webView) { (webView) in
            webView.top == (webView.superview?.top)! + 20
            webView.left == (webView.superview?.left)! + 5
            webView.right == (webView.superview?.right)! - 5
            webView.bottom == (webView.superview?.bottom)! - 5
        }
    }
    
    private func configureSubviews(){
        view.backgroundColor = UIColor.clearColor()
        webView.allowsLinkPreview = true
        
        //Enable zooming in webview
        var scriptContent = "var meta = document.createElement('meta');"
        scriptContent += "meta.name='viewport';"
        scriptContent += "meta.content='width=device-width,initial-scale=0.3, maximum-scale=2.0, user-scalable=2.0';"
        scriptContent += "document.getElementsByTagName('head')[0].appendChild(meta);"
        
        
        let uScript = WKUserScript(source: scriptContent, injectionTime: .AtDocumentEnd, forMainFrameOnly: true)
        let wkUserContentController = WKUserContentController()
        wkUserContentController.addUserScript(uScript)
        let config = WKWebViewConfiguration()
        config.userContentController = wkUserContentController
        webView = WKWebView(frame: CGRectZero, configuration: config)
        
        webView.scrollView.minimumZoomScale = 1.0
        webView.scrollView.maximumZoomScale = 2.0
        
        
    }
    
}
