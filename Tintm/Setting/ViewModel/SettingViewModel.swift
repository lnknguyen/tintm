//
//  SettingViewModel.swift
//  Tintm
//
//  Created by Nguyen Luong on 7/30/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import UIKit

enum SettingEnum {
    
}

struct SettingViewModel {
    let section = ["Sản phẩm"]
    let menu : [[String]] = [["Đến Website","Thư viện","Điều khoản","Thông tin","Góp ý & Phản hồi lỗi"]]
    
}

extension SettingViewModel : TextPresentable {
    var textColor: UIColor {
        return UIColor.appColor()
    }
    
    var headingFont: UIFont {
        return UIFont.appRegularFont(16.0)
    }
}