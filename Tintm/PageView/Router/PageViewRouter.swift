//
//  PageViewRouter.swift
//  Tintm
//
//  Created by Nguyen Luong on 8/23/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import Foundation

import Foundation
//Shared router for root view and side menu
protocol PageViewRouterProtocol {
    func pushFromPageViewToArticleView(row: Int)
    
}