//
//  PageViewController.swift
//  Tintm
//
//  Created by Nguyen Luong on 8/17/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import UIKit
import Bond

class PageViewController: UIPageViewController {
    var vm = PageViewViewModel()
    var currentViewModel : Observable<ArticleViewModel>
    var currentVc : Observable<ArticleViewController>
    let bookmarkButton = UIButton(frame: CGRectMake(0,0,35,35))
    let shareButton = UIButton(frame: CGRectMake(0,0,35,35))
    let realmManager = RealmCommandManager()
    
    override init(transitionStyle style: UIPageViewControllerTransitionStyle, navigationOrientation: UIPageViewControllerNavigationOrientation, options: [String : AnyObject]?) {
        currentViewModel = Observable(ArticleViewModel())
        currentVc = Observable(ArticleViewController())
        super.init(transitionStyle: style, navigationOrientation: navigationOrientation, options: options)
        
    }
    
    convenience init(){
        self.init(transitionStyle: .Scroll, navigationOrientation: .Horizontal, options: nil)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        dataSource = self
        navigationController?.navigationBar.translucent = false
        vm.delegate = self
        bindViewModel()
        
        setupPageControl()
        configureNavigationBar()
        
    }
    
    private func bindViewModel(){
        AlertViewManager.sharedInstance.showLoading(self, color: UIColor.appColor())
        vm.loadRelatedFeeds()
        
    }
    
    private func setupPageControl() {
        let appearance = UIPageControl.appearance()
        appearance.pageIndicatorTintColor = UIColor.lightGrayColor()
        appearance.currentPageIndicatorTintColor = UIColor.appColor()
        appearance.backgroundColor = UIColor.clearColor()
    }
    
    private func configureNavigationBar(){
        self.navigationController?.navigationBarHidden = true
        
        bookmarkButton.imageView?.frame = CGRectMake(0,0,35,35)
        bookmarkButton.imageEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5)
        bookmarkButton.imageView!.contentMode = .ScaleAspectFit
        let nonBookmarkedImg = UIImage(named: ImageName.DisabledBookmarkIcon.description)
        bookmarkButton.setImage(nonBookmarkedImg, forState: .Normal)
        let bookmark = UIBarButtonItem(customView: bookmarkButton)
        
        
        shareButton.imageView?.frame = CGRectMake(0,0,35,35)
        shareButton.imageEdgeInsets = UIEdgeInsetsMake(0, 5, 2, 5)
        shareButton.imageView!.contentMode = .ScaleAspectFit
        shareButton.setImage(UIImage(named: ImageName.ShareIcon.description), forState: .Normal)
        let share = UIBarButtonItem(customView: shareButton)
        
        self.navigationItem.rightBarButtonItems = [bookmark,share]
        
    }
    
    private func buttonHandlers(){
        let nonBookmarkedImg = UIImage(named: ImageName.DisabledBookmarkIcon.description)
        let bookmarkedImg = UIImage(named: ImageName.EnabledBookmarkIcon.description)
        
        var url = NSURL(string: "")!
        
        self.currentViewModel.value.url.observe({ (text) in
            url = NSURL(string: text)!
        })
        
        self.currentViewModel.observe { (event) in
            event.isBookmarked.observe({ (value) in
                
                if value {
                    self.bookmarkButton.setImage(bookmarkedImg, forState: .Normal)
                }
                else {
                    self.bookmarkButton.setImage(nonBookmarkedImg, forState: .Normal)
                }
                
            })
            
            event.url.observe({ (text) in
                url = NSURL(string: text)!
            })
        }
        
        
        shareButton.bnd_tap.observe {
            let rootVc = UINavigationController.rootNavigationController
            let accVc = UIActivityViewController(activityItems: [url], applicationActivities: nil)
            rootVc.presentViewController(accVc, animated: true, completion: nil)
            
        }
        
        
        bookmarkButton.bnd_tap.observe {[unowned self] in
            if (self.bookmarkButton.currentImage == nonBookmarkedImg){
                self.bookmarkButton.setImage(bookmarkedImg, forState: .Normal)
                AlertViewManager.sharedInstance.showSuccess(.Bookmarked)
                self.realmManager.update{
                    self.currentViewModel.value.realmNews?.homeObject!.isBookmark = true
                }
                
            }else{
                self.bookmarkButton.setImage(nonBookmarkedImg, forState: .Normal)
                self.realmManager.update{
                    self.currentViewModel.value.realmNews?.homeObject!.isBookmark = false
                }
            }
            
        }
    }
    
}


extension PageViewController : UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    func pageViewController(pageViewController: UIPageViewController,
                            viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        
        
        guard let vcIndex = vm.vcArray.indexOf(viewController as! ArticleViewController) else { return nil }
        
        
        let prevIndex = vcIndex - 1
        let vcArrayCount = vm.vcArray.count
        var vc : ArticleViewController
        guard prevIndex >= 0 else {
            if vcArrayCount>1 {
                vc = vm.vcArray.last!
                currentVc.next(vc)
                currentViewModel.next( vc.viewModel)
                return vc
            } else {return nil}
        }
        
        guard vcArrayCount > prevIndex else { return nil }
        vc = vm.vcArray[prevIndex]
        currentVc.next(vc)
        currentViewModel.next( vc.viewModel)
        
        return vc
    }
    
    func pageViewController(pageViewController: UIPageViewController,
                            viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        
        guard let vcIndex = vm.vcArray.indexOf(viewController as! ArticleViewController) else { return nil }
        
        let nextIndex = vcIndex + 1
        let vcArrayCount = vm.vcArray.count
        var vc : ArticleViewController
        guard vcArrayCount != nextIndex else {
            if vcArrayCount>1 {
                vc = vm.vcArray.first!
                currentVc.next(vc)
                currentViewModel.next( vc.viewModel)
                return vc
            } else {return nil}
        }
        
        guard vcArrayCount > nextIndex else { return nil }
        vc = vm.vcArray[nextIndex]
        currentVc.next(vc)
        currentViewModel.next( vc.viewModel)
        
        return vc
    }
    
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        
        return vm.vcArray.count
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        guard let firstVc = vm.vcArray.first, firstVcIndex = vm.vcArray.indexOf(firstVc) else { return 0 }
        return firstVcIndex
    }
    /*
     func pageViewController(pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
     currentVc = pageViewController.viewControllers?.first as? ArticleViewController
     currentViewModel = currentVc?.viewModel
     }
     */
}

//Different handlers or reuse function ?
extension PageViewController: PageViewAdapter{
    func successHandler(ids: [Int]) {
        vm.createVcArray()
        setupPageControl()
        if let firstVc = vm.vcArray.first {
            let router = ArticleViewRouter(vc: firstVc)
            firstVc.router = router
            setViewControllers([firstVc], direction: .Forward, animated: true, completion: nil)
            currentVc.next(firstVc)
            currentViewModel.next( firstVc.viewModel)
        }
        AlertViewManager.sharedInstance.stopLoading()
        buttonHandlers()
    }
    
    func failureHandler(err: ErrorType) {
        vm.createVcArray()
        setupPageControl()
        if let firstVc = vm.vcArray.first {
            setViewControllers([firstVc], direction: .Forward, animated: true, completion: nil)
            currentVc.next(firstVc)
            currentViewModel.next( firstVc.viewModel)
            
        }
        AlertViewManager.sharedInstance.stopLoading()
        buttonHandlers()
    }
}

extension PageViewController{
    
}
