//
//  ArticleViewControllerExtension.swift
//  Tintm
//
//  Created by Nguyen Luong on 8/11/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import UIKit
import WebKit
import Cartography

//MARK: Web view delegate
extension ArticleViewController : WKNavigationDelegate{
    func webView(webView: WKWebView, decidePolicyForNavigationAction navigationAction: WKNavigationAction, decisionHandler: (WKNavigationActionPolicy) -> Void) {
        if (navigationAction.targetFrame == nil){
            let url = navigationAction.request.URL
            let wv = WKWebView(frame: CGRectMake(0, 0, APP_WIDTH, APP_HEIGHT))
            let vc = UIViewController()
            vc.view.addSubview(wv)
            wv.loadRequest(NSURLRequest(URL: url!))
            navigationController?.pushViewController(vc, animated: true)
            //UIApplication.sharedApplication().openURL(url!)
        }
        decisionHandler(.Allow)
    }
    
}


//MARK: View model delegate
extension ArticleViewController: ArticleAdapter{
    
    func successHandler(){
        emptyView.removeFromSuperview()
        let entity = viewModel.feed
        
        let content = entity.content
        let title = entity.title
        let source = entity.sourceName
        let timestamp = entity.timestamp.timePassed
        
        //Convert to html string
        let desHtml = String.toHtml(title, sourceName: source,timeStamp: timestamp, content: content)
        
        webView.loadHTMLString(desHtml, baseURL: NSURL(fileURLWithPath: path))
        
        let realmObj = viewModel.realmManager.create(entity)
        viewModel.realmManager.write(realmObj)
        viewModel.realmNews = realmObj as? RealmNewsObject
        AlertViewManager.sharedInstance.stopLoading()
        webView.scrollView.fadeIn(5.0, delay: 5.0)
        viewModel.url.next(entity.tintmUrl)
        
    }
    
    func failureHandler(err: ErrorType){
        AlertViewManager.sharedInstance.showFailure(.NoInternetConnection)
        view.addSubview(emptyView)
        constrain(emptyView) { (view1) in
            guard let superview = view1.superview else { return }
            view1.top == superview.top
            view1.left == superview.left
            view1.right == superview.right
            view1.bottom == superview.bottom
        }
        AlertViewManager.sharedInstance.stopLoading()
    }
    
    //Handlers for load more operation
    func successLoadMoreHandler() {
        tableView.reloadData()
    }
    
    func failureLoadMore(err: ErrorType) {
        print("No related feeds")
    }
}

//MARK: Scrollview delegate
extension ArticleViewController: UIScrollViewDelegate{
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        scrollView.decelerationRate = UIScrollViewDecelerationRateNormal
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        constrainSubviews()
    }
    func scrollViewDidScroll(scrollView: UIScrollView){
        
    }
}

//MARK: Tableview delegate and data source
extension ArticleViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.relatedFeeds.value.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let inpRow = indexPath.row
        let cell = (tableView.dequeueReusableCellWithIdentifier(RELATED_NEWS_ID, forIndexPath: indexPath) as? RelatedNewsCell)!
        let entity = viewModel.relatedFeeds.value[inpRow]
        
        cell.publishData(entity)
        
        return cell
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        //return NSAttributedString(string: "Tin liên quan",attributes: [NSFontAttributeName: UIFont.appRegularFont(20)]) as? String
        if viewModel.relatedFeeds.value.count > 0{
            return "Tin liên quan"
        }
        else {return nil}
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let ye = router {
            ye.pushFromArticleViewToRelatedArticleView(indexPath.row)
        }else {
            print("nil router")
        }
    }
    
}
