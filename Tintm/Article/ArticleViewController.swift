//
//  File.swift
//  Tintm
//
//  Created by Nguyen Luong on 6/22/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import UIKit
import Cartography
import Bond
import WebKit
import RealmSwift
import Foundation
//Remember to target delegate functions in viewmodel to view controller
class ArticleViewController: UIViewController{
    var viewModel = ArticleViewModel()
    var router : ArticleViewRouter?
    let emptyView = StateView(frame: CGRectZero, state: .NoInternet)
    var webView = WKWebView()
    var tableView = UITableView(frame: CGRectZero, style: .Plain)
    var contentHeight = Observable<CGFloat>(0.0)
    let backButton = UIButton(frame: CGRectMake(0,0,25,25))
    let realmManager = RealmCommandManager()
    
    let bundle = NSBundle.mainBundle()
    var path = ""
    
    let bookmarkButton = UIButton(frame: CGRectMake(0,0,35,35))
    let shareButton = UIButton(frame: CGRectMake(0,0,35,35))
    
    typealias ArticleViewPresenter = protocol<TextPresentable,ImagePresentable>
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.whiteColor()
        tableView.delegate = self
        tableView.dataSource = self
        path =  bundle.pathForResource("default", ofType: "css")! // css path
        configureSubviews()
        constrainSubviews()
        
        viewModel.delegate = self // -> important
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        configureNavigationBar()
        
        //Fade webview in
        webView.scrollView.alpha = 0.0
        webView.addObserver(self, forKeyPath: "loading", options: .New, context: nil)
        webView.scrollView.addObserver(self, forKeyPath: "contentSize", options: .New, context: nil)
        bindViewModel()
        
        
    }
    
    
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(true)
        AlertViewManager.sharedInstance.stopLoading()
        if let news = viewModel.realmNews {
            viewModel.realmManager.write(news)
        }
        webView.removeObserver(self, forKeyPath: "loading")
        webView.scrollView.removeObserver(self, forKeyPath: "contentSize")
    }
    
    
    func constrainSubviews(){
        view.addSubview(webView)
        webView.scrollView.addSubview(tableView)
    }
    
    //Private functions
    private func configureSubviews(){
        view.backgroundColor = UIColor.clearColor()
        webView.allowsLinkPreview = true
        tableView.backgroundColor = UIColor.redColor()
        //Enable zooming in webview
        var scriptContent = "var meta = document.createElement('meta');"
        scriptContent += "meta.name='viewport';"
        scriptContent += "meta.content='width=device-width,initial-scale=0.3, maximum-scale=2.0, user-scalable=2.0';"
        scriptContent += "document.getElementsByTagName('head')[0].appendChild(meta);"
        
        let uScript = WKUserScript(source: scriptContent, injectionTime: .AtDocumentEnd, forMainFrameOnly: true)
        let wkUserContentController = WKUserContentController()
        wkUserContentController.addUserScript(uScript)
        let config = WKWebViewConfiguration()
        config.userContentController = wkUserContentController
        webView = WKWebView(frame: CGRectZero, configuration: config)
        
        webView.scrollView.minimumZoomScale = 1.0
        webView.scrollView.maximumZoomScale = 2.0
        webView.navigationDelegate = self
        webView.scrollView.delegate = self
        
        tableView.registerClass(RelatedNewsCell.self, forCellReuseIdentifier: RELATED_NEWS_ID)
        tableView.estimatedRowHeight = APP_HEIGHT/5
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.setNeedsLayout()
        tableView.backgroundColor = UIColor.lightGrayColor()
        tableView.scrollEnabled = false
    }
    
    private func bindViewModel(){
        buttonHandlers()
        
        let news = viewModel.realmManager.read(.News, dictionary: ["id":viewModel.id]) as! [RealmNewsObject]
        
        
        if (news.count > 0){
            if let news = news.first{
                AlertViewManager.sharedInstance.showLoading(self, color: UIColor.appColor())
                viewModel.realmNews = news
                webView.loadHTMLString(news.content, baseURL: NSURL(fileURLWithPath: self.path))
                
                if let obj = news.homeObject{
                    self.viewModel.url.next(obj.tintmUrl)
                    self.viewModel.isBookmarked.next(obj.isBookmark)
                }
                constrainSubviews()
                
                
            }
        }
        else
        {
            AlertViewManager.sharedInstance.showLoading(self, color: UIColor.appColor())
            viewModel.loadNewsById()
        }
        
        viewModel.loadRelatedFeeds()
    }
    
    private func configureNavigationBar(){
        self.navigationController?.navigationBarHidden = false
        
        bookmarkButton.imageView?.frame = CGRectMake(0,0,35,35)
        bookmarkButton.imageEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5)
        bookmarkButton.imageView!.contentMode = .ScaleAspectFit
        let bookmark = UIBarButtonItem(customView: bookmarkButton)
        
        
        shareButton.imageView?.frame = CGRectMake(0,0,35,35)
        shareButton.imageEdgeInsets = UIEdgeInsetsMake(0, 5, 2, 5)
        shareButton.imageView!.contentMode = .ScaleAspectFit
        shareButton.setImage(UIImage(named: ImageName.ShareIcon.description), forState: .Normal)
        let share = UIBarButtonItem(customView: shareButton)
        
        self.navigationItem.rightBarButtonItems = [bookmark,share]
    }
    
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        
        if ( keyPath! == "contentSize"){
            let height = webView.scrollView.contentSize.height
            
            AlertViewManager.sharedInstance.stopLoading()
            
            webView.scrollView.fadeIn(0.5, delay: 0)
            
            constrain(webView,tableView){ webView, tableView in
                guard let superview = webView.superview else {return}
                webView.top == superview.top
                webView.left == superview.left
                webView.right == superview.right
                (webView.bottom == superview.bottom - 5) ~ 600
            }
            tableView.frame = CGRectMake(0, height, APP_WIDTH, APP_HEIGHT)
            webView.scrollView.contentMode = .ScaleAspectFit
            webView.scrollView.contentInset = UIEdgeInsetsMake(0, 0, tableView.contentSize.height, 0)
            
        }
    }
    private func buttonHandlers(){
        let nonBookmarkedImg = UIImage(named: ImageName.DisabledBookmarkIcon.description)
        let bookmarkedImg = UIImage(named: ImageName.EnabledBookmarkIcon.description)
        
        var url = NSURL(string: "")!
        
        viewModel.isBookmarked.observe({ (value) in
            
            if value {
                self.bookmarkButton.setImage(bookmarkedImg, forState: .Normal)
            }
            else {
                self.bookmarkButton.setImage(nonBookmarkedImg, forState: .Normal)
            }
            
        })
        
        viewModel.url.observe({ (text) in
            url = NSURL(string: text)!
        })
        
        
        
        shareButton.bnd_tap.observe {
            let rootVc = UINavigationController.rootNavigationController
            let accVc = UIActivityViewController(activityItems: [url], applicationActivities: nil)
            rootVc.presentViewController(accVc, animated: true, completion: nil)
            
        }
        
        
        bookmarkButton.bnd_tap.observe {[unowned self] in
            if (self.bookmarkButton.currentImage == nonBookmarkedImg){
                self.bookmarkButton.setImage(bookmarkedImg, forState: .Normal)
                AlertViewManager.sharedInstance.showSuccess(.Bookmarked)
                self.realmManager.update{
                    self.viewModel.realmNews?.homeObject!.isBookmark = true
                }
                
            }else{
                self.bookmarkButton.setImage(nonBookmarkedImg, forState: .Normal)
                self.realmManager.update{
                    self.viewModel.realmNews?.homeObject!.isBookmark = false
                }
            }
            
        }
    }
}




