//
//  ArticleModel.swift
//  Tintm
//
//  Created by Nguyen Luong on 6/23/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import Foundation
import Bond

class ArticleModel {
    var news = NewsEntity()
    var observableTitle = Observable<String>("")
    var observableSummary = Observable<String>("")
    var observableContent = Observable<String>("")
    var observableSourceName = Observable<String>("")
    init(){
        
    }
}

