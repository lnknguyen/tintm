//
//  ArticleViewModel.swift
//  Tintm
//
//  Created by Nguyen Luong on 6/22/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import Foundation
import Bond
import Social

protocol ArticleAdapter{
    func successHandler()
    func successLoadMoreHandler()
    func failureHandler(err: ErrorType)
    func failureLoadMore(err: ErrorType)
}

class ArticleViewModel: ButtonDelegate{
    var id: Int = 0
    var feed = NewsEntity()
    let realmManager = RealmCommandManager()
    var realmNews : RealmNewsObject? = nil
    var delegate : ArticleAdapter?
    var relatedFeeds : Observable<[HomeNewsEntity]>
    
    var url : Observable<String>
    var isBookmarked : Observable<Bool>
    init(id: Int = 0){
        self.id = id
        relatedFeeds = Observable([])
        url = Observable("")
        isBookmarked = Observable(false)
    }
    
    
    func shareButtonHandler(button: UIButton, url: NSURL){
        button.bnd_tap.observe {
            let rootVc = UINavigationController.rootNavigationController
            let accVc = UIActivityViewController(activityItems: [url], applicationActivities: nil)
            rootVc.presentViewController(accVc, animated: true, completion: nil)
        }
    }
    
    //MARK: -Networking
    func loadNewsById(){
        NetworkManager.sharedInstance.getFeedDetail(id) { (res) in
            switch res.status{
            case .Success:
                if let result = res.res{
                    self.feed = result
                }
                self.delegate?.successHandler()
            case .Failure:
                if let err = res.error{
                    self.delegate?.failureHandler(err)
                }
            }
        }
    }
    
    func loadRelatedFeeds(){
        NetworkManager.sharedInstance.getRelatedFeeds(id) { (res) in
            switch res.status {
            case .Success:
                if let result = res.res{
                    self.relatedFeeds.next(result)
                    self.delegate?.successLoadMoreHandler()
                }
            case .Failure:
                if let err = res.error{
                    self.delegate?.failureLoadMore(err)
                }
            }
            
        }
    }
}



//Text presentable, img presentable
extension ArticleViewModel: TextPresentable,ImagePresentable{
    var align : NSTextAlignment { return NSTextAlignment.Center }
    var headingFont : UIFont { return UIFont(name: "Roboto-Bold", size: 20)!}
    var imageName : String { return ImageName.CancelIcon.description}
}

