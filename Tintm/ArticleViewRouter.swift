//
//  ArticleViewRouter.swift
//  Tintm
//
//  Created by Nguyen Luong on 7/29/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import Foundation

protocol ArticleViewRouterProtocol {
    func pushFromArticleViewToRelatedArticleView(row: Int)
    func popFromArticleViewToHomeView()
    func popFromArticleViewToBookmarkView()
}

