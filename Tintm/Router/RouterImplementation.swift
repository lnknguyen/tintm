//
//  RouterComponent.swift
//  Tintm
//
//  Created by Nguyen Luong on 7/29/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

//App routing will be handled here
//Router must hold weak reference to view controller
//All customizations for navigation bar should be implemented here
//Custom transitions should also be handled here

import UIKit
import SideMenu

//MARK: - Root and side menu
/* Router impl for root view router protocol*/
struct RootViewRouter: RootViewRouterProtocol{
    //Avoid circular dependency -> using weak here
    weak var sideMenuViewController : SideMenuViewController?
    
    init(vc: SideMenuViewController){
        sideMenuViewController = vc
    }
    
    /**
     Push to search view
     */
    func pushFromSideMenuToSearchView() {
        let vc = SearchViewController(style: .Plain)
        let viewModel = SearchViewModel()
        let router = SearchViewRouter(vc: vc)
        vc.router = router
        vc.viewModel = viewModel
        let navVc = sideMenuViewController?.navigationController as! UISideMenuNavigationController
        navVc.dismissViewControllerAnimated(true, completion: nil)
        let rootVc = UINavigationController.rootNavigationController
        rootVc.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: vc, action:nil )
        vc.title = "Tìm kiếm"
        rootVc.pushViewController(vc, animated: true)
    }
    
    /**
     Push to bookmark view
     */
    func pushFromSideMenuToBookmarkView() {
        let vc = BookmarkViewController(style: .Plain)
        let viewModel = BookmarkViewModel()
        let router = BookmarkViewRouter(vc: vc, vm: viewModel)
        vc.router = router
        vc.viewModel = viewModel
        let navVc = sideMenuViewController?.navigationController as! UISideMenuNavigationController
        navVc.dismissViewControllerAnimated(true, completion: nil)
        let rootVc = UINavigationController.rootNavigationController
        rootVc.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: vc, action:nil )
        vc.title = "Tin đã lưu"
        rootVc.pushViewController(vc, animated: true)
        
    }
    
    /**
     Push to setting view
     */
    func pushFromSideMenuToSettingsView() {
        
        let vc = SettingViewController()
        let viewModel = SettingViewModel()
        let router = SettingViewRouter(vc: vc, vm: viewModel)
        vc.viewModel = viewModel
        vc.router = router
        let navVc = sideMenuViewController?.navigationController as! UISideMenuNavigationController
        navVc.dismissViewControllerAnimated(true, completion: nil)
        let rootVc = UINavigationController.rootNavigationController
        rootVc.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: vc, action:nil )
        vc.title = "Cài đặt"
        rootVc.pushViewController(vc, animated: true)
    }
}

//MARK: - Home
/* Router impl for home view */
struct HomeViewRouter : HomeViewRouterProtocol {
    weak var homeViewController : HomeViewController?
    var homeViewModel : HomeViewModel?
    
    init(vc: HomeViewController, vm: HomeViewModel){
        homeViewController = vc
        homeViewModel = vm
        
    }
    
    /**
     Push to an article view
     
     - parameter row: row in which the cell is selected
     */
    func pushFromHomeViewToArticle(row: Int) {
        let vm = homeViewController?.viewModel
        if let homeViewModel = vm{
            var id : Int
            if homeViewModel.isOffline == false {
                id = homeViewModel.observableIds.value[row]
            }else {
                id = homeViewModel.offlineDataSource[row].id
            }
            let viewModel = ArticleViewModel(id: id)
            let vc = ArticleViewController()
            let router = ArticleViewRouter(vc: vc)
            vc.router = router
            vc.viewModel = viewModel
            let categoryName = homeViewModel.name
            
            //setup navigation
            let rootVc = UINavigationController.rootNavigationController
            rootVc.navigationBarHidden = false
            rootVc.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: categoryName, style: .Plain, target: nil, action: nil)
            rootVc.pushViewController(vc, animated: false)
        }else{
            fatalError("View model must not be nil")
        }
    }
    
    /**
     Push to a list of feeds
     
     - parameter row: row in which cell is selected
     */
    func pushFromHomeViewToPageView(row: Int) {
        let vm = homeViewController?.viewModel
        if let homeViewModel = vm{
            var id : Int
            if homeViewModel.isOffline == false {
                id = homeViewModel.observableIds.value[row]
            }else {
                id = homeViewModel.offlineDataSource[row].id
            }
            let viewModel = PageViewViewModel(id: id)
            let vc = PageViewController()
            //let router = ArticleViewRouter(vc: vc)
            //vc.router = router
            vc.vm = viewModel
            let categoryName = homeViewModel.name
            
            //setup navigation
            let rootVc = UINavigationController.rootNavigationController
            rootVc.navigationBarHidden = false
            rootVc.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: categoryName, style: .Plain, target: nil, action: nil)
            rootVc.pushViewController(vc, animated: false)
        }else{
            fatalError("View model must not be nil")
        }
        
    }
    
    /**
     Push to a list of feeds
     
     - parameter row: row in which cell is selected
     */
    func pushFromHomeViewToPageView(row: Int) {
        let vm = homeViewController?.viewModel
        if let homeViewModel = vm{
            var id : Int
            if homeViewModel.isOffline == false {
                id = homeViewModel.observableIds.value[row]
            }else {
                id = homeViewModel.offlineDataSource[row].id
            }
            let viewModel = PageViewViewModel(id: id)
            let vc = PageViewController()
            //let router = ArticleViewRouter(vc: vc)
            //vc.router = router
            vc.vm = viewModel
            let categoryName = homeViewModel.name
            
            //setup navigation
            let rootVc = UINavigationController.rootNavigationController
            rootVc.navigationBarHidden = false
            rootVc.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: categoryName, style: .Plain, target: nil, action: nil)
            rootVc.pushViewController(vc, animated: false)
        }else{
            fatalError("View model must not be nil")
        }

    }
    
}


//MARK: - Article
/* Router impl for article view*/
struct ArticleViewRouter : ArticleViewRouterProtocol{
    weak var articleViewController : ArticleViewController?
    //var articleViewModel : ArticleViewModel
    
    init(vc: ArticleViewController){
        articleViewController = vc
        
    }
    
    func popFromArticleViewToHomeView() {
        /* Impl later */
        let navVc = articleViewController?.navigationController
        navVc?.navigationBarHidden = true
        navVc?.popViewControllerAnimated(true)
    }
    
    func popFromArticleViewToBookmarkView() {
        /* Impl later*/
        
        let navVc = articleViewController?.navigationController
        navVc?.navigationBarHidden = false
        navVc?.popViewControllerAnimated(true)
    }
    
    func pushFromArticleViewToRelatedArticleView(row: Int) {
        let vm = articleViewController?.viewModel
        if let articleViewModel = vm{
            let id = articleViewModel.relatedFeeds.value[row].id
            let viewModel = ArticleViewModel(id: id)
            let vc = ArticleViewController()
            let router = ArticleViewRouter(vc: vc)
            vc.router = router
            vc.viewModel = viewModel
            let rootVc = UINavigationController.rootNavigationController
            rootVc.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
            let bookmark = UIBarButtonItem(title: "", style: .Plain, target: vc, action: nil)
            rootVc.navigationItem.rightBarButtonItem = bookmark
            rootVc.pushViewController(vc, animated: true)
        }else{
            fatalError("View model must not be nil")
        }

    }
}

//MARK: - Bookmark
/* Router impl for bookmark view*/

struct BookmarkViewRouter : BookmarkViewRouterProtocol{
    weak var bookmarkViewController : BookmarkViewController?
    var bookmarkViewModel : BookmarkViewModel?
    
    init(vc: BookmarkViewController, vm: BookmarkViewModel){
        bookmarkViewController = vc
        bookmarkViewModel = vm
    }
    
    func popFromBookmarkViewToHomeView() {
        /* Impl later */
    }
    
    /**
     Push to an article view
     
     - parameter row: row in which the cell is selected
     */
    func pushFromBookmarkViewToArticleView(row: Int) {
        if let bookmarkViewModel = bookmarkViewModel{
            let id = bookmarkViewModel.dataArray[row].id
            let viewModel = ArticleViewModel(id: id)
            let vc = ArticleViewController()
            let router = ArticleViewRouter(vc: vc)
            vc.router = router
            vc.viewModel = viewModel
            
            let rootVc = UINavigationController.rootNavigationController
            rootVc.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "Bookmarks", style: .Plain, target: nil, action: nil)
            let bookmark = UIBarButtonItem(title: "", style: .Plain, target: vc, action: nil)
            rootVc.navigationItem.rightBarButtonItem = bookmark
            rootVc.pushViewController(vc, animated: true)
        }else{
            fatalError("View model must not be nil")
        }
        
    }
}

//MARK: - Setting

struct SettingViewRouter : SettingRouterProtocol{
    weak var settingViewController : SettingViewController?
    var settingViewModel : SettingViewModel?
    
    init(vc: SettingViewController, vm: SettingViewModel){
        settingViewController = vc
        settingViewModel = vm
    }
    
    /**
     Push to library view
     */
    func pushToLibraryView() {
        let vc = LibraryViewController()
        vc.title = "Thư viện"
        settingViewController!.navigationController?.navigationBar.topItem!.backBarButtonItem =  UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        settingViewController!.navigationController?.pushViewController(vc, animated: true)
    }
    
    /**
     PUsh to website
     */
    func pushToWebsiteView() {
        UIApplication.sharedApplication().openURL(NSURL(string: "http://www.tintm.com/")!)
    }
    
    /**
     Push to mail composer
     */
    func pushToMailView() {
        let vc = MailViewController()
        settingViewController?.presentViewController(vc, animated: true, completion: nil)
    }
}

struct SearchViewRouter: SearchViewRouterProtocol{
    weak var searchViewController : SearchViewController?
    
    init(vc: SearchViewController){
        searchViewController = vc
    }
    
    /**
     Push to an article view
     
     - parameter row: row in which the cell is selected
     */
    func pushFromSearchViewToArticleView(row: Int) {
        let vm = searchViewController?.viewModel
        if let searchViewModel = vm{
            let id = searchViewModel.filteredRealmObject[row].id
            let viewModel = ArticleViewModel(id: id)
            let vc = ArticleViewController()
            let router = ArticleViewRouter(vc: vc)
            vc.router = router
            vc.viewModel = viewModel
            let rootVc = UINavigationController.rootNavigationController
            rootVc.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
            let bookmark = UIBarButtonItem(title: "", style: .Plain, target: vc, action: nil)
            rootVc.navigationItem.rightBarButtonItem = bookmark
            rootVc.pushViewController(vc, animated: true)
        }else{
            fatalError("View model must not be nil")
        }
    }
    
    func popFromSearchViewToHomeView() {
        /*Impl later*/
    }
}

//MARK: - Transition animation handler
//This class conforms to the transition delegate
class AnimationController: NSObject, UIViewControllerAnimatedTransitioning {
    
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?)-> NSTimeInterval {
        return 0
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        
    }
}