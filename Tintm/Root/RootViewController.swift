//
//  Root.swift
//  Tintm
//
//  Created by Nguyen Luong on 6/20/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import UIKit


import Cartography
import SideMenu

class RootViewController: UIViewController{
    var viewModel = RootViewModel()
    var pageMenu : CAPSPageMenu?
    var vcArray : [UIViewController] = []
    let refreshControl = UIRefreshControl()
    let params: [CAPSPageMenuOption] = [.ScrollMenuBackgroundColor(UIColor.appColor()),
                                        .CenterMenuItems(true),
                                        .EnableHorizontalBounce(true),
                                        .MenuItemWidthBasedOnTitleTextWidth(true),
                                        .SelectionIndicatorColor(UIColor.clearColor()),
                                        .SelectedMenuItemLabelColor(UIColor.whiteColor()),
                                        .MenuItemFont(UIFont.appRegularFont(15)),
                                        .MenuItemSeparatorRoundEdges(true)]
    
    
    let emptyView = StateView(frame: CGRectZero, state: .NoInternet)
    
    var menuLeft = UISideMenuNavigationController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.edgesForExtendedLayout = .None
        view.backgroundColor = UIColor.appColor()
        viewModel.delegate = self
        
        setupSideMenu()
        bindViewModel()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.navigationBarHidden = true
    }
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    
    private func bindViewModel(){
        AlertViewManager.sharedInstance.showLoading(self)
        viewModel.load()
    }
    
    func refresh(){
        bindViewModel()
    }
    
    private func setupSideMenu(){
        let vc = SideMenuViewController()
        let router = RootViewRouter(vc: vc)
        vc.router = router
        menuLeft.leftSide = true
        menuLeft.viewControllers = [vc]
        SideMenuManager.menuWidth = 80.0
        SideMenuManager.menuAnimationFadeStrength = 1
        SideMenuManager.menuShadowRadius = 20
        SideMenuManager.menuShadowColor = UIColor.appColor()
        SideMenuManager.menuAddPanGestureToPresent(toView: (self.navigationController?.navigationBar)!)
        SideMenuManager.menuAllowPopIfPossible = true
        SideMenuManager.menuLeftNavigationController = menuLeft
        
    }
}

//MARK: Delegated by viewmodel
extension RootViewController : RootAdapter{
    //MARK: Result handlers
    func successHandler(){
        emptyView.removeFromSuperview()
        for entity in self.viewModel.categoryArray.value{
            let viewModel = HomeViewModel(id: entity.id,name: entity.name)
            let vc = HomeViewController()
            let router = HomeViewRouter(vc: vc, vm: viewModel)
            vc.viewModel = viewModel
            if entity.id == 4 {
                vc.title = "KH-CN"
            } else {
                vc.title = entity.name
            }
            
            vc.view.backgroundColor = UIColor.whiteColor()
            vc.router = router
            let navVc = UINavigationController(rootViewController: vc)
            navVc.navigationBarHidden = true
            self.vcArray.append(navVc)
        }
        
        setupPageMenu()
        
        
    }
    
    func failureHandler(){
        //Display a temporary table view
        //Scroll the table to try loading data
        AlertViewManager.sharedInstance.showFailure(.NoInternetConnection)
        let objs = viewModel.realmManager.read(.Category, dictionary: [:]) as! [RealmCategoryObject]
        if objs.count <= 0{
            view.addSubview(emptyView)
            constrain(emptyView) { (view1) in
                guard let superview = view1.superview else { return }
                view1.top == superview.top
                view1.left == superview.left
                view1.right == superview.right
                view1.bottom == superview.bottom
            }
            
        }else {
            for entity in objs{
                if entity.id != 11 {
                    let viewModel = HomeViewModel(id: entity.id,name: entity.name,isOffline: true)
                    let vc = HomeViewController()
                    let router = HomeViewRouter(vc: vc, vm: viewModel)
                    vc.viewModel = viewModel
                    
                    if entity.id == 4 {
                        vc.title = "KH-CN"
                    } else {
                        vc.title = entity.name
                    }
                    
                    vc.view.backgroundColor = UIColor.whiteColor()
                    vc.router = router
                    let navVc = UINavigationController(rootViewController: vc)
                    navVc.navigationBarHidden = true
                    self.vcArray.append(navVc)
                }
            }
            
            setupPageMenu()
            
        }
        
        AlertViewManager.sharedInstance.stopLoading()
        self.refreshControl.endRefreshing()
    }
    
    private func setupPageMenu(){
        pageMenu = CAPSPageMenu(viewControllers: self.vcArray, frame: CGRectMake(0, STATUS_BAR_HEIGHT, APP_WIDTH, APP_HEIGHT), pageMenuOptions: params)
        view.addSubview(self.pageMenu!.view)
        /*constrain((pageMenu?.view)!) { (view1) in
         guard let superview = view1.superview else { return }
         view1.left == superview.left
         view1.right == superview.right
         view1.height == APP_HEIGHT
         view1.top == superview.top + STATUS_BAR_HEIGHT
         }*/
        
        pageMenu?.menuButton.bnd_tap.observe({
            self.presentViewController(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
        })
        
    }
    
}

