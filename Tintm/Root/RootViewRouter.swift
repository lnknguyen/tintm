//
//  RootViewRooter.swift
//  Tintm
//
//  Created by Nguyen Luong on 7/29/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import Foundation
//Shared router for root view and side menu
protocol RootViewRouterProtocol {
    func pushFromSideMenuToBookmarkView()
    func pushFromSideMenuToSettingsView()
    func pushFromSideMenuToSearchView()
}

