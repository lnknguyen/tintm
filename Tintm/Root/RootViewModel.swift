//
//  RootViewModel.swift
//  Tintm
//
//  Created by Nguyen Luong on 6/20/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//




import PromiseKit
import Bond

protocol RootAdapter {
    func successHandler()
    func failureHandler()
}

struct RootViewModel: TableViewPresentable{
    var categoryArray:  Observable<[CategoryEntity]>
    var delegate : RootAdapter?
    let realmManager = RealmCommandManager()
    init(){
        categoryArray = Observable([])
    }
    
    //MARK: -Networking

    func load(){
        NetworkManager.sharedInstance.getCategories { (res)  in
            switch res.status{
            case .Success:
                if let arr = res.res{
                    for val in arr{
                        var tempVal = val
                        if tempVal.id == 4 {
                            tempVal.name = "KH-CN"
                        }
                        let obj = self.realmManager.create(tempVal)
                        self.realmManager.write(obj)
                    }
                    self.categoryArray.next(arr)
                }
                self.delegate!.successHandler()
            case .Failure:
                self.delegate!.failureHandler()
            }
        }
    }
}
