//
//  Entity.swift
//  Tintm
//
//  Created by Nguyen Luong on 6/20/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import ObjectMapper
import Bond
import Realm

protocol Entity: Mappable{
    var id: Int { get }
}


//Entity of categories

struct CategoryEntity: Entity {
    var id : Int = 0
    var interest: Int = 0
    var name: String = ""
    var slug: String = ""
    
    init?(_ map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        id <- map["Id"]
        interest <- map["Interest"]
        name <- map["Name"]
        slug <- map["Slug"]
    }
}

//Entity to be displayed in news view

struct NewsEntity:Entity, Equatable{
    var id: Int = 0
    var categoryId: Int = 0
    var categoryName = ""
    var content = ""
    var sourceName = ""
    var sourceUrl = ""
    var summary = ""
    var timestamp = ""
    var title = ""
    var tintmUrl = ""
    init(){
        
    }
    //Mappable
    init?(_ map: Map) {
       
    }
    
    mutating func mapping(map: Map) {
        id <- map["Id"]
        categoryId <- map["CategoryId"]
        categoryName <- map["CategoryName"] 
        content <- map["Content"]
        sourceName <- map["SourceName"]
        sourceUrl <- map["SourceUrl"]
        summary <- map["Summary"]
        timestamp <- map["TimeISO"]
        title <- map["Title"]
        tintmUrl <- map["Url"]
    }
}

//Entity to be displayed in home view

struct HomeNewsEntity: Entity, Equatable {
    var id: Int = 0
    var timeInMili : Double = 0
    var categoryId : Int = 0
    var categoryName = ""
    var sourceName = ""
    var sourceUrl = ""
    var summary = ""
    var timestamp = ""
    var title = ""
    var thumbnail = ""
    var image = ""
    var related : Int = 0
    var tintmUrl = ""
    init(){
    
    }
    
    //Mappable
    init?(_ map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        id <- map["Id"]
        categoryName <- map["CategoryName"]
        categoryId <- map["CategoryId"]
        sourceName <- map["SourceName"]
        sourceUrl <- map["SourceURL"]
        summary <- map["Summary"]
        title <- map["Title"]
        thumbnail <- map["Thumbnail"]
        timestamp <- map["TimeISO"]
        timeInMili <- map["TimeInMili"]
        image <- map["Image"]
        related <- map["Related"]
        tintmUrl <- map["Url"]
    }
}

func == (lhs: HomeNewsEntity, rhs: HomeNewsEntity) -> Bool{
    return lhs.id == rhs.id
}

func == (lhs: NewsEntity, rhs: NewsEntity) -> Bool{
    return lhs.id == rhs.id
}

//MARK: Result type entity
struct ResultEntity<T> {
    var res : T?
    var status : Result<InfoCode,ErrorCode>
    var error : ErrorType?
    
    init(res: T,status: Result<InfoCode,ErrorCode>,error: ErrorType){
        self.res = res
        self.status = status
        self.error = error
    }
    
    init(res: T){
        self.res = res
        status = .Success(InfoCode.DoneLoadingNews)
        error = nil
    }
    
    init(error: ErrorType){
        self.error = error
        status = .Failure(ErrorCode.UnknownError)
        res = nil
    }
}

