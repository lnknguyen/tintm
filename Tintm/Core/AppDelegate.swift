//
//  AppDelegate.swift
//  Tintm
//
//  Created by Nguyen Luong on 6/17/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import UIKit
import RealmSwift
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let realmManager = RealmCommandManager()
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
      
        let rootVc = RootViewController()
        //let rootVc = PageViewController()
        //let rootVc = SearchViewController(style: .Grouped)
        let rootNavVc = UINavigationController(rootViewController: rootVc)
        rootNavVc.navigationBarHidden = true
        rootNavVc.navigationBar.translucent = true
        rootNavVc.navigationBar.barStyle = .Black
        rootNavVc.customizeNavigationBar()
        window = UIWindow(frame: UIScreen.mainScreen().bounds)
        window?.rootViewController = rootNavVc
        window?.backgroundColor = UIColor.whiteColor()
        window?.makeKeyAndVisible()
        
        
        
        //printFonts()
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        //Delete all unbookmarked home realm that are created before 2 days ago
        realmManager.clearAllFeedsBeforeDate(.Home, value: 3)
        realmManager.clearAllFeedsBeforeDate(.News, value: 3)
    }
    
    func printFonts() {
        let fontFamilyNames = UIFont.familyNames()
        for familyName in fontFamilyNames {
            print("------------------------------")
            print("Font Family Name = [\(familyName)]")
            let names = UIFont.fontNamesForFamilyName(familyName )
            print("Font Names = [\(names)]")
        }
    }
    


}

