//
//  WebService.swift
//  Tintm
//
//  Created by Nguyen Luong on 6/20/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//


import Alamofire
import PromiseKit
import ObjectMapper

//Singleton web service
//Cache all by default
class WebService{
    let realmManager = RealmCommandManager()
    let manager : Alamofire.Manager
    static let sharedInsance = WebService()
    
    private init() {
        // Create a shared URL cache
        let memoryCapacity = 500 * 1024 * 1024; // 500 MB
        let diskCapacity = 500 * 1024 * 1024; // 500 MB
        let cache = NSURLCache(memoryCapacity: memoryCapacity, diskCapacity: diskCapacity, diskPath: "shared_cache")
        
        // Create a custom configuration
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        let defaultHeaders = Alamofire.Manager.sharedInstance.session.configuration.HTTPAdditionalHeaders
        configuration.HTTPAdditionalHeaders = defaultHeaders
        configuration.requestCachePolicy = .UseProtocolCachePolicy // this is the default
        configuration.URLCache = cache
        
        // Create own manager instance that uses custom configuration
        manager = Alamofire.Manager(configuration: configuration)
    }
    
    /**
     Return all categories with their properties
     
     - returns: List of category entites
     */
    func queryAllCategories() -> Promise<[CategoryEntity]>{
        let url = APIPath.Category.URLPath
        return Promise{ fulfill, reject in
            manager.request(.GET, url)
                .validate()
                .responseJSON { (res) in
                    if res.result.error == nil {
                        if let category = Mapper<CategoryEntity>().mapArray(res.result.value as! [AnyObject]){
                            fulfill(category)
                        }
                    }
                    else{
                        reject(res.result.error!)
                    }
                    
            }
        }
        
    }
    
    /**
     Return all highlight news with respectable category
     
     - returns: List of home news entity
     */
    func queryAllHighlightNews() -> Promise<[Int]>{
        let url = APIPath.Hotnews.URLPath
        return Promise{ fulfill, reject in
            manager.request(.GET, url)
                .validate()
                .responseJSON { (res) in
                    if res.result.error == nil {
                        fulfill((res.result.value?.valueForKey("1"))! as! [Int])
                        
                    }
                    else {
                        reject(res.result.error!)
                    }
            }
        }
    }
    
    /**
     Return news lists by category id
     
     - parameter id: category's id
     
     - returns: list of news ids ordered by ranking
     */
    func queryHighlightNewsIdByCategoryId(id: Int)->Promise<[Int]>{
        let url = APIPath.Hotnews.URLPath
        return Promise{ fulfill, reject in
            manager.request(.GET, url)
                .validate()
                .responseJSON { (res) in
                    if res.result.error == nil {
                        if let temp = res.result.value?.valueForKey(String(id)) {
                            let idList : [Int] = temp as! [Int]
                            fulfill(idList)
                        }
                        else{
                            reject(NSError(domain: "Fail to retrieve category", code: 400, userInfo: nil))
                        }
                        
                    }
                    else {
                        reject(res.result.error!)
                    }
            }
        }
        
    }
    
    /**
     Return news by Id
     - parameter id: news' id
     
     - returns: news content
     */
    
    func queryNewsDetailById(id: Int) -> Promise<NewsEntity>{
        let url = APIPath.News.URLPath
        return Promise{ fulfill, reject in
            manager.request(.GET, url+String(id))
                .validate()
                .responseJSON { (res) in
                    if res.result.error == nil{
                        
                        if let hotnews = Mapper<NewsEntity>().map(res.result.value){
                            fulfill(hotnews)
                        }
                        else {
                            reject(NSError(domain: "Fail to get news", code: 400, userInfo: nil))
                        }
                    }
                    else{
                        reject(res.result.error!)
                    }
            }
        }
    }
    
    /**
     Load every 5 feeds in the category
     
     - parameter id:   array of feeds Id
     - parameter offset: offset of feeds
     
     - returns: List of 5 news, begin from offset
     */
    
    func queryForMoreFeedsFromCategory(ids: [Int], offset: Int) -> Promise<[HomeNewsEntity]> {
        let url = APIPath.NewsList.URLPath
        var idChain = ""
        
        //Calculate endpoint from offset
        //Considering edge cases
        if (offset < ids.count-1){
            //Chain news id
            
            let endPoint = offset + 9 <= ids.count-1 ? offset + 9 : ids.count - 1
            let subIds = ids[offset...endPoint]
            
            let strs = subIds.map { (id) -> String in
                return String(id)
            }
            
            
            
            //Concatenate id, seperated by commas
            for str in strs {
                idChain += str
                idChain += ","
            }
            
            if idChain.characters.count > 0 {
                idChain = idChain.substringToIndex(idChain.endIndex.predecessor())
            }
            
            return Promise{fulfill, reject in
                manager.request(.GET,url + idChain)
                    .validate()
                    .responseJSON{ (res) in
                        if (res.result.error == nil){
                            if (res.result.value as! [AnyObject]).count > 0{
                                if let newsEntity = Mapper<HomeNewsEntity>().mapArray(res.result.value as! [AnyObject]){
                                    
                                    fulfill(newsEntity)
                                }
                                else{
                                    reject(NSError(domain: "Fail loading more home news", code: 400, userInfo: nil))
                                }
                            }
                        } else {
                            reject(res.result.error!)
                        }
                }
            }
        } else {
            //Reject when offset value is bigger than ids array range
            return Promise{fulfill, reject in
                reject(NSError(domain: "Overbound", code: 403, userInfo: nil))
            }
        }
        
    }
    
    /**
     Load a single feed based on its id
     
     - parameter id:   feeds Id
     - returns: a single feeds entity
     */
    
    func queryForSingleFeed(id: Int) -> Promise<HomeNewsEntity> {
        let url = APIPath.NewsList.URLPath
        return Promise{fulfill, reject in
            manager.request(.GET, url + String(id))
                .validate()
                .responseJSON{ (res) in
                    if (res.result.error == nil){
                        if (res.result.value as! [AnyObject]).count > 0{
                            if let newsEntity = Mapper<HomeNewsEntity>().mapArray(res.result.value as! [AnyObject] ){
                                fulfill(newsEntity[0])
                            }
                                
                            else{
                                reject(NSError(domain: "Fail loading home news", code: 400, userInfo: nil))
                            }
                        }
                    }
                    else {
                        reject(res.result.error!)
                    }
            }
        }
    }
    
    /**
     Load a list of related feeds based on a feed's id
     
     - parameter id:     feed's id
     - parameter limit:  number of related feeds need to be queried
     - parameter lastId: for paging
     
     - returns: list of related feeds
     */
    func queryForRelatedFeeds(id: Int, limit: Int = 3, lastId : Int = 0) -> Promise<[HomeNewsEntity]>{
        let url = APIPath.Related.URLPath
        let params = ["id":id,"limit":limit]
        return Promise{ fulfill, reject in
            manager.request(.GET, url, parameters: params)
                .validate()
                .responseJSON{ (res) in
                    if (res.result.error == nil){
                        if (res.result.value as! [AnyObject]).count > 0 {
                            if let entities = Mapper<HomeNewsEntity>().mapArray(res.result.value as! [AnyObject]){
                                fulfill(entities)
                            }
                        }
                        else {
                            reject(NSError(domain: "Fail loading related news", code: 400, userInfo: nil))
                        }
                    }
                    else {
                        reject(res.result.error!)
                    }
            }
            
        }
    }
    
}
