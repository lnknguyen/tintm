//
//  BookmarkViewController.swift
//  Tintm
//
//  Created by Nguyen Luong on 7/28/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import UIKit

class BookmarkViewController: UITableViewController, ButtonDelegate{

    var viewModel : BookmarkViewModel?
    var router : BookmarkViewRouter?
    
    override init(style: UITableViewStyle) {
        super.init(style: style)
        
        tableView.registerClass(BookmarkCell.self, forCellReuseIdentifier: BOOKMARK_CELL_ID)
        
        tableView.showsVerticalScrollIndicator = false
        tableView.alwaysBounceVertical = false
        
        tableView.estimatedRowHeight = APP_HEIGHT/5
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.setNeedsLayout()
        
        
    }
    
    convenience init(){
        self.init(style: .Plain)
    }
    
    override func viewDidLoad() {
        
    }
    override func viewWillAppear(animated: Bool) {
        tableView.reloadData()
        navigationController?.navigationBarHidden = false
        
        if viewModel?.dataArray.count == 0 {
            tableView.backgroundView = StateView(frame: CGRectMake(0, 0, APP_WIDTH, APP_HEIGHT), state: .Empty)
            tableView.separatorColor = UIColor.clearColor()
        }
    }
    
   
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    //MARK: Delegate and data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel!.dataArray.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(BOOKMARK_CELL_ID) as! BookmarkCell
        let object  = viewModel!.dataArray[indexPath.row]
        
        cell.publishData(object)
        cell.constrainSubview()
        cell.delegate = self
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        router?.pushFromBookmarkViewToArticleView(indexPath.row)
    }
    
    //MARK: Delegate method for button
    func didTapButtonInsideCell(sender: UITableViewCell) {
        let cell = sender as! BookmarkCell
        tableView.beginUpdates()
        switch cell.state{
        case .Expanded:
            cell.collapseView()
        case .Collapsed:
            cell.expandView()
            
        }
        tableView.layoutIfNeeded()
        tableView.endUpdates()
    }
    
    
}