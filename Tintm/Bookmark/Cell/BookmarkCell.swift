//
//  BookmarkCell.swift
//  Tintm
//
//  Created by Nguyen Luong on 7/29/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import UIKit
import Cartography

class BookmarkCell: UITableViewCell{
    
    var delegate : ButtonDelegate?
    var viewModel = HomeViewModel()
    var state = CellState.Collapsed
    var titleView = UILabel()
    var summaryView = UILabel()
    var sourceView = UILabel()
    var timestampView = UILabel()
    var expandButton = UIButton()
    var group = ConstraintGroup()
    
    typealias Presenter = protocol<TextPresentable>
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: "")
        self.selectionStyle = .None
        
        self.addSubview(titleView)
        self.addSubview(sourceView)
        self.addSubview(timestampView)
        self.addSubview(expandButton)
        
        configureSubview(viewModel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        delegate = nil
        if state == .Expanded {
            collapseView()
        }
    }
    
    func constrainSubview(){
        constrain(titleView, sourceView, timestampView, expandButton, replace: group) { (view1, view2, view3, view4) in
            guard let superview = view1.superview else { return }
            
            view1.top == superview.top + 5
            view1.right == superview.right - 10
            view1.left == superview.left + 10
            
            view2.top == view1.bottom + 5
            view2.left == view1.left
            view2.height == 15
            
            view3.top == view2.top
            view3.left == view2.right + 5
            view3.bottom == view2.bottom
            
            view4.right == superview.right - 30
            view4.width == 30
            view4.height == view4.width
            
            (view2.bottom <= superview.bottom - 5) ~ 100
            (view4.bottom <= superview.bottom - 5) ~ 100
        }
        
    }
    
    func configureSubview(presenter: Presenter){
        //Set subviews background color to white to avoid blending operation in cell
        //See more: https://medium.com/ios-os-x-development/perfect-smooth-scrolling-in-uitableviews-fd609d5275a5#.ymbe3iumc
        
        titleView.font = presenter.headingFont
        summaryView.font = presenter.summaryFont
        sourceView.font = presenter.sourceNameFont
        timestampView.font = presenter.timeStampFont
        
        sourceView.textColor = presenter.sourceNameColor
        timestampView.textColor = presenter.timeStampColor
        
        for subview in self.subviews{
            if subview is UILabel{
                if let subview = subview as? UILabel
                {
                    subview.numberOfLines = presenter.numberOfLines
                    subview.lineBreakMode = presenter.lineBreakMode
                    subview.sizeToFit()
                    subview.backgroundColor = presenter.backgroundColor
                }
            }
        }
        
        summaryView.numberOfLines = presenter.numberOfLines
        summaryView.lineBreakMode = presenter.lineBreakMode
        summaryView.sizeToFit()
        summaryView.backgroundColor = presenter.backgroundColor
        
        expandButton.setImage(UIImage(named: ImageName.ExpandIcon.description), forState: .Normal)
        expandButton.imageView!.backgroundColor = UIColor.whiteColor()
        expandButton.imageEdgeInsets = UIEdgeInsetsMake(10, 5, 0, 5)
        expandButton.bnd_tap.observe { [unowned self] in
            if let _delegate = self.delegate{
                _delegate.didTapButtonInsideCell(self)
            }
        }
        
    }
    
        
    func publishData(feed: RealmHomeObject){
        timestampView.text = feed.timestamp.timePassed
        titleView.text = feed.title
        summaryView.text = feed.summary
        sourceView.text = feed.sourceName
    }
    
    func expandView() {
        state.switchState()
        expandButton.setImage(UIImage(named: ImageName.CollapseIcon.description), forState: .Normal)
        expandButton.imageEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5)
        
        self.addSubview(summaryView)
        summaryView.alpha = 0
        constrain(summaryView,sourceView, expandButton) { (view1, view2, view3) in
            guard let superview = view1.superview else { return }
            //view2.top == (view1.bottom + 10) ~ 400
            view1.top == (view2.bottom + 5) ~ 400
            view1.left == superview.left + 5
            view1.right == superview.right - 5
            view1.bottom <= (superview.bottom - 5) ~ 600
            
            view3.top == view1.bottom + 5
            view3.bottom <= (superview.bottom - 5) ~ 800
        }
        
        UIView.animateWithDuration(0.5) {
            self.summaryView.alpha = 1
        }
        
    }
    
    
    func collapseView() {
        state.switchState()
        expandButton.setImage(UIImage(named: ImageName.ExpandIcon.description), forState: .Normal)
        expandButton.imageEdgeInsets = UIEdgeInsetsMake(10, 5, 0, 5)
        summaryView.removeFromSuperview()
        constrain(sourceView){view1 in
            view1.bottom <= (view1.superview?.bottom)! - 5
        }
    }
}