//
//  BookmarkViewModel.swift
//  Tintm
//
//  Created by Nguyen Luong on 7/28/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import Foundation
import RealmSwift

struct BookmarkViewModel {
    let realmManager = RealmCommandManager()
    var dataArray : [RealmHomeObject]{
        get {
            let temp : [RealmHomeObject ] =  realmManager.read(.Home, dictionary: ["isBookmark":true]) as! [RealmHomeObject]
            //return temp.sort({$0.0.timestamp.toDate().compare($0.1.timestamp.toDate()) == .OrderedAscending})
            return temp
        }
    }
    
}

extension BookmarkViewModel : TextPresentable {
}