//
//  Enum.swift
//  Tintm
//
//  Created by Nguyen Luong on 7/15/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import Foundation

//MARK: State of home cell enum
enum CellState
{
    case Collapsed
    case Expanded
}

extension CellState
{
    mutating func switchState(){
        switch self{
        case Collapsed:
            self = Expanded
        case Expanded:
            self = Collapsed
        }
    }
}


//MARK: Web services enum
enum APIPath {
    case Category
    case Hotnews
    case News
    case NewsList
    case Related
}

extension APIPath {
    var URLPath: String {
        switch self {
        case .Category:
            return "\(BASE_API)/category"
        case .Hotnews:
            return "\(BASE_API)/hotnews"
        case .News:
            return "\(BASE_API)/news/"
        case .NewsList:
            return "\(BASE_API)/news/list/"
        case .Related:
            return "\(BASE_API)/news/related"
        }
    }
}



//MARK: Result type enum
enum Result<InfoCode,ErrorCode>{
    case Success(InfoCode)
    case Failure(ErrorCode)
}

//MARK: Image name enum
//Note: Remember to add value here everytime adding a new image into casset
enum ImageName : String,CustomStringConvertible{
    case AppIcon
    case CollapseIcon
    case ExpandIcon
    case ScrollToTopIcon
    case CancelIcon
    case EnabledBookmarkIcon
    case DisabledBookmarkIcon
    case MenuIcon
    case BookmarkIcon
    case SettingIcon
    case SearchIcon
    case ShareIcon
    
    var description: String{
        switch self{
        case .AppIcon : return "tintm-place-holder"
        case .CollapseIcon : return "collapse-icon"
        case .ExpandIcon : return "expand-icon"
        case .ScrollToTopIcon : return "scroll-to-top-icon"
        case .CancelIcon : return "cancel-icon"
        case .EnabledBookmarkIcon : return "enabled-bookmark-icon"
        case .DisabledBookmarkIcon : return "disabled-bookmark-icon"
        case .MenuIcon : return "menu-icon"
        case .BookmarkIcon : return "bookmark-icon"
        case .SettingIcon : return "settings-icon"
        case .SearchIcon : return "search-icon"
        case .ShareIcon : return "share-icon"
        }
    }
}

//MARK: Info and error code
enum InfoCode {
    case DoneLoadingNews
    case Bookmarked
    case MailSent
    case DoneLoadMore
}

enum ErrorCode : ErrorType{
    case NoInternetConnection
    case UnknownError
    case MailFailed
}
