//
//  AlertView.swift
//  Tintm
//
//  Created by Nguyen Luong on 7/9/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import Foundation
import SwiftyDrop
import NVActivityIndicatorView

struct AlertViewManager {
    let activityIndicator : NVActivityIndicatorView
    
    static let sharedInstance = AlertViewManager()
    
    private init(){
        activityIndicator = NVActivityIndicatorView(frame: CGRectMake(100, 200, 50, 50), type: .BallSpinFadeLoader)
        activityIndicator.center = CGPointMake(APP_WIDTH/2, APP_HEIGHT/2)
    }
    
    func showSuccess(code :InfoCode) {
        switch code {
        case .Bookmarked:
            Drop.down("Đã lưu", state: .Success)
        case .MailSent:
             Drop.down("Email đã được gửi", state: .Success)
        case .DoneLoadingNews:
            Drop.down("Đã load hết tin", state: .Info)
        case .DoneLoadMore:
            Drop.down("Đang tải thêm tin", state: .Info, duration: 1.0)
        }
    }
    
    func showFailure(code: ErrorCode) {
        switch code{
        case .NoInternetConnection:
             Drop.down("Không có kết nối mạng", state: .Error)
        case .UnknownError:
             Drop.down("Không biết là lỗi gì, nhưng mà nói chung là có lỗi đó", state: .Error)
        case .MailFailed:
             Drop.down("Có lỗi khi gửi mail", state: .Error)
        }
    }
    
    //Show and hide loading indicator
    func showLoading(vc: UIViewController, color: UIColor = UIColor.whiteColor()) {
        vc.view.addSubview(activityIndicator)
        activityIndicator.color = color
        activityIndicator.startAnimation()
    }
    
    func stopLoading(){
        activityIndicator.stopAnimation()
    }
}