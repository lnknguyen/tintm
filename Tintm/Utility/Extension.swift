//
//  Extension.swift
//  Tintm
//
//  Created by Nguyen Luong on 6/20/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import UIKit

import Social


//MARK: Font extension
extension UIFont{
    
    class func appRegularFont(fontSize: CGFloat)->UIFont{
        return UIFont(name: "Roboto-Regular", size: fontSize)!
        
    }
    
    class func appBoldFont(fontSize: CGFloat)->UIFont{
        return UIFont(name: "Roboto-Bold", size: fontSize)!
    }
    
    class func appItalicFont(fontSize: CGFloat)->UIFont{
        return UIFont(name: "Roboto-Italic", size: fontSize)!
    }
}

//MARK: String extension
extension String{
    
    //Calculate time passed from timestamp
    var timePassed : String {
        let dateFormatter = NSDateFormatter()
        let localeStr = "en_US_POSIX"
        dateFormatter.locale = NSLocale(localeIdentifier: localeStr)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        
        let date = dateFormatter.dateFromString(self)
        let currentDate = NSDate()
        
        let dayHourMinuteSecond: NSCalendarUnit = [.Day, .Hour, .Minute, .Second]
        if let _date = date {
            let difference = NSCalendar.currentCalendar().components(dayHourMinuteSecond, fromDate: _date, toDate: currentDate, options: [])
            
            let seconds = "\(difference.second) giây trước"
            let minutes = "\(difference.minute) phút trước"
            let hours = "\(difference.hour)h trước "
            let days = "\(difference.day) ngày trước "
            
            if difference.day    > 0 { return days }
            if difference.hour   > 0 { return hours }
            if difference.minute > 0 { return minutes }
            if difference.second > 0 { return seconds }
        }
        return ""
    }
    
    
    
    //Append header to content and convert to a html string
    //Extension method to convert a string to a html
    static func toHtml(title: String, sourceName: String, timeStamp: String, content: String) -> String{
        
        return "<html>\n<head>\n   <meta charset=\"utf-8\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=0.3, maximum-scale=1.0, user-scalable=0\"><link rel=\"stylesheet\" href=\"default.css\"></head><body><div id=\"main\"><div tm-type=title>\(title)</div>\n<div class=\"inline-container\"><div id=\"source\">\(sourceName)</div><div id=\"timestamp\">\(timeStamp.timePassed)</div></div> \(content)</div></body>\n</html>"
    }
    
    //Extension method to convert a string to NSDate
    func toDate() -> NSDate {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        if let date = dateFormatter.dateFromString(self){
            return date
        }
        print("Fail to convert string to date, returning current date instead")
        return NSDate()
    }
    
}

//MARK: Color extension
extension UIColor {
    class func appColor() -> UIColor{
        return UIColor(red: 179.0/255, green: 61.0/255, blue: 61.0/255, alpha: 1.0)
    }
    
    class func silverColor() -> UIColor{
        return UIColor(red: 192.0/255, green: 192.0/255, blue: 192.0/255, alpha: 1.0)
    }
}

//MARK: UIView extension
extension UIView {
    var originX : CGFloat{
        return self.frame.origin.x
    }
    
    var originY : CGFloat{
        return self.frame.origin.y
    }
    
    
    var selfWidth : CGFloat{
        return self.frame.size.width
    }
    
    var selfHeight : CGFloat{
        return self.frame.size.height
    }
}

//MARK: UINavigationController extension
extension UINavigationController {
    func customizeNavigationBar() {
        self.navigationBar.backgroundColor = UIColor.appColor()
        self.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationBar.barTintColor = UIColor.appColor()
        self.navigationBar.translucent = true
        self.navigationBar.opaque = false
    }
    
    class var rootNavigationController : UINavigationController {
        return (UIApplication.sharedApplication().keyWindow?.rootViewController as? UINavigationController)!
    }
}

//MARK: NSDate extension
extension NSDate {
    func isGreaterThan(date: NSDate, unit: NSCalendarUnit, amount: Int) -> Bool{
        
        let dayHourMinuteSecond: NSCalendarUnit = [.Second,.Minute, .Day, .Hour, .Minute, .Year]
        
        let difference = NSCalendar.currentCalendar().components(dayHourMinuteSecond, fromDate: self, toDate: date, options: [])
        
        switch unit{
        case NSCalendarUnit.Second:
            return difference.second > amount
        case NSCalendarUnit.Minute:
            return difference.minute > amount
        case NSCalendarUnit.Hour:
            return difference.hour > amount
        case NSCalendarUnit.Day:
            return difference.day > amount
        case NSCalendarUnit.Month:
            return difference.month > amount
        case NSCalendarUnit.Year:
            return difference.year > amount
        default:
            return false
        }
    }
    
    func isSmallerThan(date: NSDate, unit: NSCalendarUnit, amount: Int) -> Bool {
        return !isGreaterThan(date, unit: unit, amount: amount)
    }

}

//Custom animations
extension UIView {
    func fadeIn(duration: NSTimeInterval = 1.0, delay: NSTimeInterval = 0.0, completion: ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animateWithDuration(duration, delay: delay, options: UIViewAnimationOptions.CurveEaseIn, animations: {
            self.alpha = 1.0
            }, completion: completion)  }
    
    func fadeOut(duration: NSTimeInterval = 1.0, delay: NSTimeInterval = 0.0, completion: (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animateWithDuration(duration, delay: delay, options: UIViewAnimationOptions.CurveEaseIn, animations: {
            self.alpha = 0.0
            }, completion: completion)
    }
}

//MARK: - Custom activity controller
extension UIActivityViewController {
    
    class func defaultActivityController(string: String, url: NSURL, sender: UINavigationController) -> UIActivityViewController {
        let vc = UIActivityViewController(activityItems: [string,url], applicationActivities: nil)
        vc.excludedActivityTypes = [UIActivityTypePostToWeibo, UIActivityTypePostToTencentWeibo, UIActivityTypePostToTwitter]
        return vc
    }
    
    //Custom provider
    class CustomProvider : UIActivityItemProvider {
        
        var facebookMessage : String!
        var twitterMessage : String!
        var emailMessage : String!
        
        init(placeholderItem: AnyObject, facebookMessage : String, twitterMessage : String, emailMessage : String) {
            super.init(placeholderItem: placeholderItem)
            self.facebookMessage = facebookMessage
            self.twitterMessage = twitterMessage
            self.emailMessage = emailMessage
        }
        
        override func item() -> AnyObject {
            switch (self.activityType!) {
            case UIActivityTypePostToFacebook:
                return self.facebookMessage
            case UIActivityTypePostToTwitter:
                return self.twitterMessage
            case UIActivityTypeMail:
                return self.emailMessage
            default:
                return "Any Message"
            }
        }
    }
}


