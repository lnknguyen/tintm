//
//  Constant.swift
//  Tintm
//
//  Created by Nguyen Luong on 6/17/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import UIKit

let BASE_API = "http://sm.smartapp.tech/m"
let STATUS_BAR_HEIGHT = CGFloat(20.0)
var APP_WIDTH : CGFloat { get { return  UIScreen.mainScreen().bounds.size.width } }
var APP_HEIGHT : CGFloat {get { return UIScreen.mainScreen().bounds.size.height } }
let IMAGE_RATIO  : Double = 9/16
//Cell reuse id
let HOME_CELL_TYPE_ONE_ID = "homeCellTypeOne"
let HOME_CELL_TYPE_TWO_ID = "homeCellTypeTwo"
let HOME_CELL_TYPE_THREE_ID = "homeCellTypeThree"
let BOOKMARK_CELL_ID = "bookmarkCell"
let ARTICLE_CELL_ID = "articleCell"
let SETTING_CELL_ID = "settingCell"
let SEARCH_CELL_ID = "searchCell"
let RELATED_NEWS_ID = "relatedCell"