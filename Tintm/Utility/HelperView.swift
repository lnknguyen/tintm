//
//  HelperView.swift
//  Tintm
//
//  Created by Nguyen Luong on 8/1/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import UIKit
import Cartography

protocol StateAdapter {
    func needToRefresh()
}

//This view will be displayed when no data is available yet
//Will delegate a method to the main view to refresh the network connection
//as well as the data source

enum ViewState{
    case Empty
    case NoInternet
}

class StateView : UIView {
    var state: ViewState
   
    override init(frame: CGRect) {
        state = .Empty
        super.init(frame: frame)
        self.backgroundColor = UIColor.whiteColor()
        
    }
    
    convenience init(frame: CGRect, state: ViewState){
        self.init(frame: frame)
        self.state = state
        
        let stateLabel = StateLabel(frame: CGRectMake(0,60,APP_WIDTH,60))
        stateLabel.config(state)
        self.addSubview(stateLabel)
        constrain(stateLabel) { (view1) in
            guard let superview = view1.superview else { return }
            
            view1.center == superview.center
            
        }
        
        stateLabel.center = self.center
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}

class StateLabel : UILabel{
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.font = UIFont.appBoldFont(20)
        self.textAlignment = .Center
       
    }
    
    func config(state: ViewState){
        
        switch state {
        case .Empty:
            self.attributedText = NSAttributedString(string: "Chưa có dữ liệu", attributes: [NSForegroundColorAttributeName:UIColor.appColor()])
        case .NoInternet:
            self.attributedText = NSAttributedString(string: "Không có kết nối", attributes: [NSForegroundColorAttributeName:UIColor.appColor()])
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}