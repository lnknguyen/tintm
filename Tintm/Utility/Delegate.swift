//
//  Delegate.swift
//  Tintm
//
//  Created by Nguyen Luong on 7/14/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import UIKit

protocol ButtonDelegate {
    func didTapButtonInsideCell(sender: UITableViewCell)
    func handler()
}

extension ButtonDelegate {
    func didTapButtonInsideCell(sender: UITableViewCell) {}
    func handler() {}
}
