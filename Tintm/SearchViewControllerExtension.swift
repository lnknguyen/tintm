//
//  SearchViewControllerExtension.swift
//  Tintm
//
//  Created by Nguyen Luong on 8/7/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import UIKit

extension SearchViewController: UISearchResultsUpdating{
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        viewModel!.filterContentForSearchText(searchController.searchBar.text!)
        tableView.reloadData()
    }
}

extension SearchViewController{
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.active && searchController.searchBar.text != "" {
            return viewModel!.filteredRealmObject.count
        }
        return 0
        
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(SEARCH_CELL_ID, forIndexPath: indexPath) as! SearchCell
        let object  = viewModel!.filteredRealmObject[indexPath.row]
        
        cell.publishData(object)
        cell.constrainSubview()
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        router?.pushFromSearchViewToArticleView(indexPath.row)
    }
    
}
