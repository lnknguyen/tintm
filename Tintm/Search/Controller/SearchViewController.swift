//
//  SearchViewController.swift
//  Tintm
//
//  Created by Nguyen Luong on 8/7/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import UIKit

class SearchViewController: UITableViewController{
    let searchController = UISearchController(searchResultsController: nil)
    var viewModel : SearchViewModel?
    var router: SearchViewRouter?
    
    override init(style: UITableViewStyle) {
        super.init(style: style)
        tableView.showsVerticalScrollIndicator = false
        tableView.alwaysBounceVertical = false
        
        tableView.estimatedRowHeight = APP_HEIGHT/5
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.setNeedsLayout()
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBarHidden = false
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.loadViewIfNeeded()
        definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar
        tableView.registerClass(SearchCell.self, forCellReuseIdentifier: SEARCH_CELL_ID)
    }
    
}

