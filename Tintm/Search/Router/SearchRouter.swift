//
//  SearchRouter.swift
//  Tintm
//
//  Created by Nguyen Luong on 8/7/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import Foundation

protocol SearchViewRouterProtocol {
    func pushFromSearchViewToArticleView(row: Int)
    func popFromSearchViewToHomeView()
}
