//
//  SearchViewModel.swift
//  Tintm
//
//  Created by Nguyen Luong on 8/7/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import Foundation

struct SearchViewModel {
    var filteredRealmObject = [RealmHomeObject]()
    let realmManager = RealmCommandManager()
    
    mutating func filterContentForSearchText(searchText: String, scope : String = "All"){
        filteredRealmObject = realmManager.search(.Home, dictionary: ["title" : searchText]) as! [RealmHomeObject]
        
    }

    
}